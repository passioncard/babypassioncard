//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PassionCardServices
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblFileMaster
    {
        public int RecordID { get; set; }
        public string HeaderFileID { get; set; }
        public Nullable<System.DateTime> HeaderFileCreationDate { get; set; }
        public string FooterFileID { get; set; }
        public Nullable<int> FooterRecordTotals { get; set; }
        public string FileName { get; set; }
        public System.DateTime RecordCreatedDate { get; set; }
        public int FileType { get; set; }
        public int UserID { get; set; }
    }
}
