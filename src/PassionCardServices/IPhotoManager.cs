﻿// -----------------------------------------------------------------------
// <copyright file="IPhotoManager" company="Manoj Patil">
// Copyright (c) 2017 All Rights Reserved
// </copyright>
// <author>Manoj Patil</author>
// <date>22/02/2017 10:39:57 AM</date>
// <summary></summary>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace PassionCardServices
{
    [ServiceContract]
    interface IPhotoManager
    {
        /// <summary>
        /// Picture upload with idnumber and DOB  
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fileName"></param>
        /// <param name="fileContents"></param>
        [WebInvoke(UriTemplate = "UploadPhoto/{id}/{fileName}/{dob}", Method = "POST")]
        StatusEntity UploadPhoto(string id, string fileName, string dob,  Stream fileContents);


        //POST operation for Set Password.
        [OperationContract]
        [WebInvoke(UriTemplate = "GetStatus/{id}/{dob}", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusEntity GetStatus(string id, string dob);



    }
}
