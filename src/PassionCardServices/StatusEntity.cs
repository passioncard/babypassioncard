﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PassionCardServices
{

    public class StatusEntity
    {
        [DataMember]
        public string RecordExist { get; set; } 

        [DataMember]
        public string PictureStatus { get; set; } 

        [DataMember]
        public string Gender { get; set; }

        [DataMember]
        public string StatusFlg { get; set; }

        [DataMember]
        public string StatusMsg { get; set; }

        [DataMember]
        public string StatusUpDt { get; set; }
    }
}