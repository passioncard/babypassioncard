﻿using PassionCardServices.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;

namespace PassionCardServices
{
    //[ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class PhotoManager : IPhotoManager
    {

        //[WebInvoke(UriTemplate = "UploadPhoto/{id}/{fileName}", Method = "POST")]
        public StatusEntity UploadPhoto(string id, string fileName, string dob, Stream fileContents)
        {
            var status = new StatusEntity();
            try
            {
                id = RemoveUnncessaryCharacters(id);
                var dt = StringToDate(dob);
                #region  setting read from app config
                //use local path for image save 
                string localPathToStoreImage = string.Empty;
                //if the flag is true from setting then it will save the picture into database
                bool isrequiredToSaveInDB = false;

                isrequiredToSaveInDB = Convert.ToBoolean(ConfigurationManager.AppSettings["IsRequiredToUpdatePictureInDB"]);
                localPathToStoreImage = ConfigurationManager.AppSettings["LocalPathToSaveImage"].ToString(); ;

                status.Gender = "isrequiredToSaveInDB=>" + isrequiredToSaveInDB;

                status.Gender += "*localPathToStoreImage=>" + localPathToStoreImage;
                isrequiredToSaveInDB = true;
                #endregion

                byte[] buffer = new byte[32768];
                MemoryStream ms = new MemoryStream();
                int bytesRead, totalBytesRead = 0;
                do
                {
                    bytesRead = fileContents.Read(buffer, 0, buffer.Length);
                    totalBytesRead += bytesRead;

                    ms.Write(buffer, 0, bytesRead);
                } while (bytesRead > 0);

                // Save the photo on database.
                using (PCAEntities data = new PCAEntities())
                {
                    var cardEntity = data.tblCardMasters.Where(x => x.IDNumber.EndsWith(id) && x.Birthday.Day == dt.Day && x.Birthday.Month == dt.Month && x.Birthday.Year == dt.Year).FirstOrDefault();
                    if (cardEntity != null)
                    {
                        if (isrequiredToSaveInDB)
                            cardEntity.Picture = ms.ToArray();
                        cardEntity.PictureUpdated = true;
                        //27 Feb 2017 : flag for status : used enum in admin 
                        cardEntity.RecordStatus = "PR";
                        cardEntity.SendInPrintFile = (int)SendInPrintFile.Yes;
                        cardEntity.RecordStatusUpdateDt = DateTime.Now;
                        if (!string.IsNullOrEmpty(localPathToStoreImage))
                        {
                            try
                            {
                                if (!Directory.Exists(localPathToStoreImage))
                                    Directory.CreateDirectory(localPathToStoreImage);

                                string pathWithName = Path.Combine(localPathToStoreImage, cardEntity.IDNumber + ".jpg");
                                cardEntity.PictureFile = cardEntity.IDNumber + ".jpg";

                                using (Image image = Image.FromStream(ms))
                                {
                                    image.Save(pathWithName, ImageFormat.Jpeg);
                                }
                            }
                            catch (Exception)
                            {

                            }
                        }
                        data.SaveChanges();
                    }
                }

                ms.Close();
                Console.WriteLine("Uploaded file {0} with {1} bytes", id, totalBytesRead);

                return status;
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
                return status;
            }
        }

        private string RemoveUnncessaryCharacters(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                    return string.Empty;
                if (id.Count() > 0)
                {
                    while (true)
                    {
                        if (id.StartsWith("T"))
                            id = id.Substring(1);
                        else if (id.StartsWith("X"))
                            id = id.Substring(1);
                        else
                            break;
                    }
                    return id;
                }
                else
                {
                    return id;
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
                return id;
            }
        }

        public enum SendInPrintFile
        {
            No,
            Yes,
            Print,
            CANUpdated
        }



        /// <summary>
        /// get the picture status and the gender of the baby
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public StatusEntity GetStatus(string id, string dob)
        {
            id = RemoveUnncessaryCharacters(id);

            var dt = StringToDate(dob);
            var status = new StatusEntity();
            status.RecordExist = "false";
            status.Gender = "Invalid";

            try
            {
                using (PCAEntities data = new PCAEntities())
                {
                    // Retrieve the last record.
                    var entity = data.tblCardMasters.Where(x => x.IDNumber.EndsWith(id) && x.Birthday.Day == dt.Day && x.Birthday.Month == dt.Month && x.Birthday.Year == dt.Year).FirstOrDefault();

                    if (entity != null)
                    {
                        //if (entity.Birthday.Day == dt.Day && entity.Birthday.Month == dt.Month && entity.Birthday.Year == dt.Year)
                        {
                            status.RecordExist = "true";
                            status.PictureStatus = entity.PictureUpdated.ToString();
                            status.Gender = GetGender(entity.Gender);
                            status.StatusFlg = entity.RecordStatus;
                            status.StatusMsg = GetStatusMessage(entity.RecordStatus, entity.RecordStatusUpdateDt);
                            status.StatusUpDt = entity.RecordStatusUpdateDt.HasValue ? entity.RecordStatusUpdateDt.Value.ToShortDateString() : "";
                        }
                    }
                }
            }
            catch (Exception edx)
            {
                ExceptionHelper.ExceptionTrack(edx);
            }
            return status;
        }

        private string GetStatusMessage(string recordStatus, DateTime? date)
        {
            string retunString = string.Empty;
            switch (recordStatus)
            {
                case "PR":
                    retunString = string.Format("Your application was submitted successfully on {0}.", date.Value.ToShortDateString());
                    break;
                case "SP":
                    retunString = string.Format("Your application was submitted successfully on {0}.", date.Value.ToShortDateString());
                    break;
                case "PC":
                    retunString = string.Format("Your application was submitted successfully on {0}.", date.Value.ToShortDateString());
                    break;
                case "RD":
                    retunString = string.Format("Your application was submitted successfully on {0}.", date.Value.ToShortDateString());
                    break;
                default:
                    retunString = "We did not receive your application. Please proceed to Upload Photo tab to submit your application";
                    break;
            }
            return retunString;
        }


        /// <summary>
        ///  define from gender 000 : Invalid , 001 : Female,  002 : Male
        /// </summary>
        private static string GetGender(string gender)
        {
            string returnString = string.Empty;
            switch (gender)
            {
                case "000":
                    returnString = "Invalid";
                    break;
                case "001":
                    returnString = "Female";
                    break;
                case "002":
                    returnString = "Male";
                    break;

            }
            return returnString;
        }


        private DateTime StringToDate(string dateStr)
        {
            if (dateStr.Length >= 10)
            {
                DateTime dt1 = DateTime.Now;
                //DateTime.TryParse(dateStr, out dt1);
                if (dateStr.Contains('-'))
                {
                    var arr = dateStr.Split('-');
                    dt1 = Convert.ToDateTime(arr[2] + "-" + arr[1] + "-" + arr[0]);

                }
                return dt1;
            }
            else
            {
                //TODO cehck : is it valid to insert current date
                return DateTime.Now;
            }
        }

    }
}