﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace PassionCardPublic.Helpers
{
    public static class ImageHelper
    {
        public static byte[] CropImage(byte[] content, int x, int y, int width, int height)
        {
            try
            {
                using (MemoryStream stream = new MemoryStream(content))
                {
                    return CropImage(stream, x, y, width, height);
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
                return null;
            }
        }

        public static byte[] CropImage(Stream content, int x, int y, int width, int height)
        {
            try
            {
                //Parsing stream to bitmap
                using (Bitmap sourceBitmap = new Bitmap(content))
                {
                    //Get new dimensions
                    double sourceWidth = Convert.ToDouble(sourceBitmap.Size.Width);
                    double sourceHeight = Convert.ToDouble(sourceBitmap.Size.Height);
                    Rectangle cropRect = new Rectangle(x, y, width, height);

                    //Creating new bitmap with valid dimensions
                    using (Bitmap newBitMap = new Bitmap(cropRect.Width, cropRect.Height))
                    {
                        using (Graphics g = Graphics.FromImage(newBitMap))
                        {
                            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                            g.SmoothingMode = SmoothingMode.HighQuality;
                            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                            g.CompositingQuality = CompositingQuality.HighQuality;

                            g.DrawImage(sourceBitmap, new Rectangle(0, 0, newBitMap.Width, newBitMap.Height), cropRect, GraphicsUnit.Pixel);

                            return GetBitmapBytes(newBitMap);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
                return null;
            }
        }

        public static byte[] GetBitmapBytes(Bitmap source)
        {
            try
            {
                //Settings to increase quality of the image
                ImageCodecInfo codec = ImageCodecInfo.GetImageEncoders()[4];
                EncoderParameters parameters = new EncoderParameters(1);
                parameters.Param[0] = new EncoderParameter(Encoder.Quality, 100L);

                //Temporary stream to save the bitmap
                using (MemoryStream tmpStream = new MemoryStream())
                {
                    source.Save(tmpStream, codec, parameters);

                    //Get image bytes from temporary stream
                    byte[] result = new byte[tmpStream.Length];
                    tmpStream.Seek(0, SeekOrigin.Begin);
                    tmpStream.Read(result, 0, (int)tmpStream.Length);

                    return result;
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
                return null;
            }
        }
    }
}

public static class CropImageHelper
{
    public static bool SaveExifImage(Image img,string path)
    {
        foreach (var prop in img.PropertyItems)
        {
            if (prop.Id == 0x0112) //value of EXIF
            {
                int orientationValue = img.GetPropertyItem(prop.Id).Value[0];
                RotateFlipType rotateFlipType = GetOrientationToFlipType(orientationValue);
                img.RotateFlip(rotateFlipType);
                img.Save(path);
                return true;
            }
        }
        return false;
    }

    private static RotateFlipType GetOrientationToFlipType(int orientationValue)
    {
        RotateFlipType rotateFlipType = RotateFlipType.RotateNoneFlipNone;

        switch (orientationValue)
        {
            case 1:
                rotateFlipType = RotateFlipType.RotateNoneFlipNone;
                break;
            case 2:
                rotateFlipType = RotateFlipType.RotateNoneFlipX;
                break;
            case 3:
                rotateFlipType = RotateFlipType.Rotate180FlipNone;
                break;
            case 4:
                rotateFlipType = RotateFlipType.Rotate180FlipX;
                break;
            case 5:
                rotateFlipType = RotateFlipType.Rotate90FlipX;
                break;
            case 6:
                rotateFlipType = RotateFlipType.Rotate90FlipNone;
                break;
            case 7:
                rotateFlipType = RotateFlipType.Rotate270FlipX;
                break;
            case 8:
                rotateFlipType = RotateFlipType.Rotate270FlipNone;
                break;
            default:
                rotateFlipType = RotateFlipType.RotateNoneFlipNone;
                break;
        }

        return rotateFlipType;
    }

    /// <summary>
    /// Rotate the given image file according to Exif Orientation data
    /// </summary>
    /// <param name="sourceFilePath">path of source file</param>
    /// <param name="targetFilePath">path of target file</param>
    /// <param name="targetFormat">target format</param>
    /// <param name="updateExifData">set it to TRUE to update image Exif data after rotation (default is TRUE)</param>
    /// <returns>The RotateFlipType value corresponding to the applied rotation. If no rotation occurred, RotateFlipType.RotateNoneFlipNone will be returned.</returns>
    public static RotateFlipType RotateImageByExifOrientationData(string sourceFilePath, string targetFilePath, ImageFormat targetFormat, bool updateExifData = true)
    {
        // Rotate the image according to EXIF data
        var bmp = new Bitmap(sourceFilePath);
        RotateFlipType fType = RotateImageByExifOrientationData(bmp, updateExifData);
        if (fType != RotateFlipType.RotateNoneFlipNone)
        {
            bmp.Save(targetFilePath, targetFormat);
        }
        return fType;
    }

    /// <summary>
    /// Rotate the given bitmap according to Exif Orientation data
    /// </summary>
    /// <param name="img">source image</param>
    /// <param name="updateExifData">set it to TRUE to update image Exif data after rotation (default is TRUE)</param>
    /// <returns>The RotateFlipType value corresponding to the applied rotation. If no rotation occurred, RotateFlipType.RotateNoneFlipNone will be returned.</returns>
    public static RotateFlipType RotateImageByExifOrientationData(Image img, bool updateExifData = true)
    {
        int orientationId = 0x0112;
        var fType = RotateFlipType.RotateNoneFlipNone;
        if (img.PropertyIdList.Contains(orientationId))
        {
            var pItem = img.GetPropertyItem(orientationId);
            fType = GetRotateFlipTypeByExifOrientationData(pItem.Value[0]);
            if (fType != RotateFlipType.RotateNoneFlipNone)
            {
                //img.RotateFlip(fType);
                // Remove Exif orientation tag (if requested)
                //if (updateExifData) img.RemovePropertyItem(orientationId);
            }
        }
        return fType;
    }

    /// <summary>
    /// Return the proper System.Drawing.RotateFlipType according to given orientation EXIF metadata
    /// </summary>
    /// <param name="orientation">Exif "Orientation"</param>
    /// <returns>the corresponding System.Drawing.RotateFlipType enum value</returns>
    public static RotateFlipType GetRotateFlipTypeByExifOrientationData(int orientation)
    {
        switch (orientation)
        {
            case 1:
            default:
                return RotateFlipType.RotateNoneFlipNone;
            case 2:
                return RotateFlipType.RotateNoneFlipX;
            case 3:
                return RotateFlipType.Rotate180FlipNone;
            case 4:
                return RotateFlipType.Rotate180FlipX;
            case 5:
                return RotateFlipType.Rotate90FlipX;
            case 6:
                return RotateFlipType.Rotate90FlipNone;
            case 7:
                return RotateFlipType.Rotate270FlipX;
            case 8:
                return RotateFlipType.Rotate270FlipNone;
        }
    }
}