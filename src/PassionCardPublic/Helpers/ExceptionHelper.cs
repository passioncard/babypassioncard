﻿using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace PassionCardPublic.Helpers
{
    public class ExceptionHelper
    {
        public static Logger Instance { get; private set; }
        static ExceptionHelper()
        {
            LogManager.ReconfigExistingLoggers();
            Instance = LogManager.GetCurrentClassLogger();
        }

        public static void ExceptionTrack(Exception ex)
        {
            //Instance.ErrorException("Exception :", ex);
            StackTrace st = new StackTrace(ex, true);
            StackFrame[] frames = st.GetFrames();
            foreach (StackFrame frame in frames)
            {
                string ExceptionDetails = string.Format("File Name: {0}\n Method: {1}\n(Line: {2}, Column: {3})\nMessage: {4}", frame.GetFileName(), frame.GetMethod().Name, frame.GetFileLineNumber(), frame.GetFileColumnNumber(), ex.Message);
                Console.WriteLine(ExceptionDetails);
                if (frame.GetFileLineNumber() != 0 && frame.GetFileColumnNumber() != 0)
                {
                    //System.Windows.Forms.MessageBox.Show(
                    //    ExceptionDetails,
                    //    GlobalProperties.strHeaderMess + "Exception Message",
                    //     System.Windows.Forms.MessageBoxButtons.OK,
                    //      System.Windows.Forms.MessageBoxIcon.Warning);
                    break;
                }
            }
            Instance.Error("Exception :", ex);

            #region "Commented Used Nlog for Logging"
            //StackTrace st = new StackTrace(ex, true);
            //StackFrame[] frames = st.GetFrames();

            //// Iterate over the frames extracting the information you need
            //foreach (StackFrame frame in frames)
            //{
            //    string ExceptionDetails = string.Format("File Name: {0}\n Method: {1}\n(Line: {2}, Column: {3})\nMessage: {4}", frame.GetFileName(), frame.GetMethod().Name, frame.GetFileLineNumber(), frame.GetFileColumnNumber(), ex.Message);
            //    Console.WriteLine(ExceptionDetails);
            //    //if (frame.GetFileLineNumber() != 0 && frame.GetFileColumnNumber() != 0)
            //    //{
            //    //    System.Windows.Forms.MessageBox.Show(
            //    //        ExceptionDetails,
            //    //        Constant.MsgBoxCaption,
            //    //         System.Windows.Forms.MessageBoxButtons.OK,
            //    //          System.Windows.Forms.MessageBoxIcon.Warning);
            //    //}

            //    LogFile(frame.GetFileName(), frame.GetMethod().Name, frame.GetFileLineNumber(), frame.GetFileColumnNumber(), ex.Message);
            //}
            #endregion
        }
        #region "Commented Used Nlog for Logging"
        // For Exception LogFile
        //public static void LogFile(string sExceptionFileName, string sMethodName, int sLineNumber, int sColumnNumber, string sMessage)
        //{
        //    StreamWriter log;
        //    string FilePath = System.Windows.Forms.Application.StartupPath.ToString() + "\\AlumnErrorlog.txt";
        //    if (!File.Exists(FilePath))
        //    {
        //        log = new StreamWriter(FilePath);
        //    }
        //    else
        //    {
        //        log = File.AppendText(FilePath);
        //    }

        //    // Write to the file:

        //    log.WriteLine("Data Time:" + DateTime.Now);

        //    log.WriteLine("Exception File Name:" + sExceptionFileName);

        //    log.WriteLine("Method Name:" + sMethodName);

        //    log.WriteLine("Line No:" + sLineNumber);

        //    log.WriteLine("Column No.:" + sColumnNumber);

        //    log.WriteLine("Exception Message:" + sMessage);

        //    log.WriteLine("-------------------------------------------------------------");
        //    log.WriteLine("-------------------------------------------------------------");

        //    // Close the stream:

        //    log.Close();

        //}
        #endregion
    }
}