﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace PassionCardPublic.Helpers
{
    public class FileHelper
    {
        public static void SaveFile(byte[] content, string path)
        {
            try
            {
                string filePath = GetFileFullPath(path);
                if (!Directory.Exists(Path.GetDirectoryName(filePath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(filePath));
                }

                //Save file
                using (FileStream str = File.Create(filePath))
                {
                    str.Write(content, 0, content.Length);
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
            }
        }

        public static string GetFileFullPath(string path)
        {
            try
            {
                string relName = path.StartsWith("~") ? path : path.StartsWith("/") ? string.Concat("~", path) : path;

                string filePath = relName.StartsWith("~") ? HostingEnvironment.MapPath(relName) : relName;

                return filePath;
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
                return "";
            }
        }
    }
}