﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PassionCardPublic.Models;
using PassionCardPublic.Helpers;
using System.Net;
using System.IO;
using System.Drawing;
using System.Configuration;
using System.Runtime.Serialization.Json;

using Newtonsoft.Json;


namespace PassionCardPublic.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Menu = "Index";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(UploadPictureModel model, System.Web.HttpPostedFileBase importFile1)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var responseValue = string.Empty;
                    string serviceURl = ConfigurationManager.AppSettings["ServiceURL"];

                    if (importFile1 != null && importFile1.ContentLength > 0 && ValidateImage(importFile1))
                    {
                        var fileName = model.IDNumber + ".jpg";

                        var imagePath = System.IO.Path.Combine(Server.MapPath("~/Images/"), fileName);
                        if (!System.IO.Directory.Exists(Server.MapPath("~/Images/")))
                            System.IO.Directory.CreateDirectory(Server.MapPath("~/Images/"));

                        importFile1.SaveAs(imagePath);

                        Image img = new Bitmap(imagePath);
                        var imagePathexif = System.IO.Path.Combine(Server.MapPath("~/Images/"), (model.IDNumber + "_exif" + ".jpg"));
                        //var exifResult = CropImageHelper.RotateImageByExifOrientationData(imagePath, imagePathexif, System.Drawing.Imaging.ImageFormat.Jpeg) != RotateFlipType.RotateNoneFlipNone;
                        //if (exifResult)
                        //{
                        //    img.Dispose();
                        //    img = new Bitmap(imagePathexif);
                        //}

                        var exifResult = CropImageHelper.SaveExifImage(img, imagePathexif);
                        if (exifResult)
                        {
                            img.Dispose();
                            img = new Bitmap(imagePathexif);
                        }

                        System.Console.WriteLine(img.Width + " x " + img.Height);

                        double xFactor = (double)img.Width / (double)model.OriginalImageWidth;
                        double yFactor = (double)img.Height / (double)model.OriginalImageHeight;
                        img.Dispose();

                        byte[] imageBytes = null;
                        if (exifResult)
                        {
                            imageBytes = System.IO.File.ReadAllBytes(imagePathexif);
                        }
                        else
                        {
                            imageBytes = System.IO.File.ReadAllBytes(imagePath);
                        }


                        byte[] croppedImage = ImageHelper.CropImage(imageBytes, (int)(model.cropPointX * xFactor), (int)(model.cropPointY * yFactor), (int)(model.imageCropWidth * xFactor), (int)(model.imageCropHeight * xFactor));

                        string tempFolderName = Server.MapPath("~/TempImages");
                        string fileName1 = fileName = model.IDNumber + ".jpg"; //Path.GetFileName(imagePath);

                        try
                        {
                            FileHelper.SaveFile(croppedImage, Path.Combine(tempFolderName, fileName1));

                            string requestUrl = string.Format("{0}/UploadPhoto/{1}/{2}/{3}", serviceURl, model.IDNumber, fileName, model.DOB).Trim();

                            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestUrl);
                            request.Method = "POST";
                            request.ContentType = "text/plain";
                            //request.ContentType = "multipart/form-data; boundary=" + boundary;


                            request.ContentLength = croppedImage.Length;
                            //request.KeepAlive = true;
                            //request.Credentials = System.Net.CredentialCache.DefaultCredentials;
                            //request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";

                            using (Stream requestStream = request.GetRequestStream())
                            {
                                // Send the file as body request.
                                requestStream.Write(croppedImage, 0, croppedImage.Length);
                                requestStream.Close();
                            }

                            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                            {
                                if (response.StatusCode == HttpStatusCode.OK)
                                {
                                    Console.WriteLine("HTTP/{0} {1} {2}", response.ProtocolVersion, (int)response.StatusCode, response.StatusDescription);

                                    Stream stream = response.GetResponseStream();

                                    if (stream != null)
                                        using (var reader = new StreamReader(stream))
                                        {
                                            responseValue = reader.ReadToEnd();
                                        }
                                }
                                else
                                {
                                    ModelState.AddModelError("errorMessage", "We are currently experiencing some technical issues. Please try again later");
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            //Log an error   
                            ModelState.AddModelError("errorMessage", "We are currently experiencing some technical issues. Please try again later");
                            ExceptionHelper.ExceptionTrack(ex);
                            //return new HttpStatusCodeResult((int)HttpStatusCode.InternalServerError);
                        }
                        return RedirectToAction("Success", "Home");
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                    return View();
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
                return View();
            }
        }

        private bool ValidateImage(HttpPostedFileBase importFile1)
        {
            try
            {
                var allowedFormats = new[]
                {
                System.Drawing.Imaging.ImageFormat.Jpeg,
                System.Drawing.Imaging.ImageFormat.Png,
                System.Drawing.Imaging.ImageFormat.Gif,
                System.Drawing.Imaging.ImageFormat.Bmp
            };

                using (var img = Image.FromStream(importFile1.InputStream))
                {
                    return allowedFormats.Contains(img.RawFormat);
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
                return false;
            }
        }

        public ActionResult CardStatus()
        {
            ViewBag.Menu = "Status";
            return View();
        }

        /// <summary>
        /// retuns the status of the baby pictures
        /// </summary>
        /// <returns></returns>
        public JsonResult GetBabyStatus(string icNumber, string dob)
        {
            try
            {
                var responseValue = string.Empty;
                string serviceURl = ConfigurationManager.AppSettings["ServiceURL"];
                if (!string.IsNullOrEmpty(serviceURl))
                {
                    string requestUrl = string.Format("{0}/GetStatus/{1}/{2}", serviceURl, icNumber, dob).Trim();

                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestUrl);
                    request.Method = "POST";
                    request.ContentType = "application/json";
                    request.ContentLength = 0;

                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            Console.WriteLine("HTTP/{0} {1} {2}", response.ProtocolVersion, (int)response.StatusCode, response.StatusDescription);

                            Stream stream = response.GetResponseStream();

                            if (stream != null)
                            {
                                using (var reader = new StreamReader(stream))
                                {
                                    responseValue = reader.ReadToEnd();
                                }
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                return Json(responseValue, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
                return null;
            }
        }

        public ActionResult About()
        {
            ViewBag.Menu = "About";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Menu = "Contact";

            return View();
        }

        public ActionResult Success()
        {
            ViewBag.Menu = "Index";

            return View();
        }

        public ActionResult FAQ()
        {
            ViewBag.Menu = "FAQ";

            return View();
        }

        public ActionResult GuidLines()
        {
            ViewBag.Menu = "Index";

            return View();
        }
    }
}