﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PassionCardPublic.Models
{
    public class UploadPictureModel
    {
        [Required]
        [MinLength(4)]
        public string IDNumber { get; set; }

        [Required]
        public string DOB { get; set; }
        public string PictureFileName { get; set; }
        public string Gender { get; set; }

        public int cropPointX { get; set; }
        public int cropPointY { get; set; }
        public int imageCropWidth { get; set; }
        public int imageCropHeight { get; set; }

        public int OriginalImageHeight { get; set; }
        public int OriginalImageWidth { get; set; }

        public int ImageHeight { get; set; }
        public int ImageWidth { get; set; }
    }
}