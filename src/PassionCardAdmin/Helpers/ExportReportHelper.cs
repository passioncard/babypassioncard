﻿using Ionic.Zip;
using Microsoft.Reporting.WebForms;
using PassionCardAdmin.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace PassionCardAdmin.Helpers
{
    public static class ExportReportHelper
    {
        /// <summary>
        /// Report explorer helper.
        /// </summary>
        /// <param name="localReport"></param>
        /// <param name="OutputType"></param>
        /// <param name="outPutFileName"></param>
        /// <param name="isLandScap"></param>
        /// <param name="extension"></param>
        /// <param name="Response"></param>
        public static void ReportExportHelper(LocalReport localReport, string OutputType, string outPutFileName, bool isLandScap, string extension, HttpResponseBase Response)
        {
            try
            {
                string reportType = OutputType;
                string mimeType;
                string encoding;
                string fileNameExtension;

                //The DeviceInfo settings should be changed based on the reportType
                //http://msdn2.microsoft.com/en-us/library/ms155397.aspx

                string deviceInfo = "";
                if (isLandScap)
                {
                    deviceInfo =
                "<DeviceInfo>" +
                "  <OutputFormat>PDF</OutputFormat>" +
                "  <PageWidth>11in</PageWidth>" +
                "  <PageHeight>8.5in</PageHeight>" +
                "  <MarginTop>0.2in</MarginTop>" +
                "  <MarginLeft>0.2in</MarginLeft>" +
                "  <MarginRight>0.2in</MarginRight>" +
                "  <MarginBottom>0.2in</MarginBottom>" +
                "</DeviceInfo>";
                }
                else
                {
                    deviceInfo =
                  "<DeviceInfo>" +
               "  <OutputFormat>PDF</OutputFormat>" +
               "  <PageWidth>8.5in</PageWidth>" +
               "  <PageHeight>11in</PageHeight>" +
               "  <MarginTop>0.2in</MarginTop>" +
               "  <MarginLeft>0.2in</MarginLeft>" +
               "  <MarginRight>0.2in</MarginRight>" +
               "  <MarginBottom>0.2in</MarginBottom>" +
               "</DeviceInfo>";
                }

                Warning[] warnings;
                string[] streams;
                byte[] renderedBytes;

                //Render the report
                renderedBytes = localReport.Render(
                    reportType,
                    deviceInfo,
                    out mimeType,
                    out encoding,
                    out fileNameExtension,
                    out streams,
                    out warnings);

                Response.Clear();
                Response.ContentType = mimeType;
                Response.AddHeader("content-disposition", "attachment; filename=" + outPutFileName + "." + extension);
                Response.BinaryWrite(renderedBytes);
                Response.End();
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
            }
        }

        internal static void ReportWithImagesZip(LocalReport localReport, string OutputType, string outPutFileName, bool isLandScap, string extension, HttpResponseBase Response, List<ExportToXlsModel> List, int recordID)
        {
            try
            {
                ZipFile zip = new ZipFile();

                #region Render Excel

                string reportType = OutputType;
                string mimeType;
                string encoding;
                string fileNameExtension;

                //The DeviceInfo settings should be changed based on the reportType
                //http://msdn2.microsoft.com/en-us/library/ms155397.aspx

                string deviceInfo = "";
                if (isLandScap)
                {
                    deviceInfo =
                "<DeviceInfo>" +
                "  <OutputFormat>PDF</OutputFormat>" +
                "  <PageWidth>11in</PageWidth>" +
                "  <PageHeight>8.5in</PageHeight>" +
                "  <MarginTop>0.2in</MarginTop>" +
                "  <MarginLeft>0.2in</MarginLeft>" +
                "  <MarginRight>0.2in</MarginRight>" +
                "  <MarginBottom>0.2in</MarginBottom>" +
                "</DeviceInfo>";
                }
                else
                {
                    deviceInfo =
                  "<DeviceInfo>" +
               "  <OutputFormat>PDF</OutputFormat>" +
               "  <PageWidth>8.5in</PageWidth>" +
               "  <PageHeight>11in</PageHeight>" +
               "  <MarginTop>0.2in</MarginTop>" +
               "  <MarginLeft>0.2in</MarginLeft>" +
               "  <MarginRight>0.2in</MarginRight>" +
               "  <MarginBottom>0.2in</MarginBottom>" +
               "</DeviceInfo>";
                }

                Warning[] warnings;
                string[] streams;
                byte[] renderedBytes;

                //Render the report
                renderedBytes = localReport.Render(
                    reportType,
                    deviceInfo,
                    out mimeType,
                    out encoding,
                    out fileNameExtension,
                    out streams,
                    out warnings);

                #endregion
                zip.AddEntry(outPutFileName + "." + extension, renderedBytes);

                var filepath = ConfigurationManager.AppSettings["LocalPathToSaveImage"].ToString();
                foreach (var item in List)
                {

                    if (File.Exists(Path.Combine(filepath, item.IDNumber + ".jpg")))
                        zip.AddFile(Path.Combine(filepath, item.IDNumber + ".jpg"), "");
                }

                var zipMs = new MemoryStream();
                zip.Save(zipMs);
                byte[] fileData = zipMs.ToArray();

                #region SaveToServerPath

                var localpath = ConfigurationManager.AppSettings["ServerPathToSaveFiles"].ToString();
                if (!Directory.Exists(localpath)) Directory.CreateDirectory(localpath);
                string localfilename = Path.Combine(localpath, recordID + ".zip");
                File.WriteAllBytes(localfilename, fileData);

                #endregion

                #region Download

                string fileName = "PrintFile_" + string.Format("{0:yyyyMMdd}", DateTime.Now) + ".zip";
                zipMs.Seek(0, SeekOrigin.Begin);
                zipMs.Flush();
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment; filename=" + fileName + "");
                Response.ContentType = "application/zip";
                Response.BinaryWrite(fileData);
                Response.End();

                #endregion
                
                
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
            }
        }
    }
}