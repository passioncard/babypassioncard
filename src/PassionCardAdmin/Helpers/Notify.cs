﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionCardAdmin.Helpers
{
    public class Notify
    {
        public NotifyLevel Level { get; set; }

        public string Message { get; set; }

        public Notify(NotifyLevel level, string message)
        {
            Level = level;
            Message = message;
        }
    }

    public enum NotifyLevel
    {
        Success = 0,
        Error = 1,
        Info = 2,
        Warning = 3,
    }
}