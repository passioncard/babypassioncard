﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace PassionCardAdmin.Helpers
{
    public class GenericListParams
    {
        private ListSortDirection? _direction;

        public int Limit { get; set; }
        public int Start { get; set; }
        public int Page { get; set; }
        public string Sort { get; set; }
        public string Filter { get; set; }

        //custom property required for applying defaults (as enum has only 2 values)
        public ListSortDirection Direction
        {
            get
            {
                if (_direction.HasValue)
                    return _direction.Value;
                return ListSortDirection.Ascending;
            }
            set
            {
                _direction = value;
            }
        }

        public NameValueCollection Params { get; private set; }

        public GenericListParams()
        { }

        public GenericListParams(NameValueCollection form = null)
        {
            if (form == null)
                form = new NameValueCollection();

            ApplyDefaults();
            Init(form);
        }

        public GenericListParams(NameValueCollection form, GenericListParams defaults)
        {
            ApplyDefaults(defaults);
            Init(form);
        }

        public void Init(NameValueCollection form)
        {
            if (form["sidx"] != null)
                this.Sort = form["sidx"].ToLower();

            if (form["sord"] != null)
            {
                if (form["sord"].Equals("asc", StringComparison.InvariantCultureIgnoreCase) ||
                    form["sord"].Equals("ascending", StringComparison.InvariantCultureIgnoreCase))
                {
                    this.Direction = ListSortDirection.Ascending;
                }
                else
                    this.Direction = ListSortDirection.Descending;
            }

            if (form["rows"] != null)
            {
                var limit = 0;
                int.TryParse(form["rows"], out limit);
                this.Limit = limit;
            }

            if (form["page"] != null)
            {
                var start = 0;
                int.TryParse(form["page"], out start);
                this.Page = start > 0 ? start : 1;
                start = start > 1 ? start * this.Limit - this.Limit : 0;
                this.Start = start;

            }

            if (form["Filter"] != null)
            {
                this.Filter = form["Filter"];
            }

            Params = form;
        }

        public void ApplyDefaults(GenericListParams overrides = null)
        {
            if (overrides == null)
                overrides = new GenericListParams();

            this.Start = (overrides.Start > 0 ? overrides.Start : 0);
            this.Page = (overrides.Limit > 0 ? overrides.Page : 1);
            this.Limit = (overrides.Page > 0 ? overrides.Limit : 10);
            this.Sort = (!string.IsNullOrEmpty(overrides.Sort) ? overrides.Sort : "id");
            this.Direction = (overrides._direction.HasValue ? overrides._direction.Value : ListSortDirection.Descending);
            this.Filter = overrides.Filter;
            //Params = new NameValueCollection(),
        }
    }

    public static class ParamsHelper
    {
        /// <summary>
        /// parse int for a request form param
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static int GetIntParam(string param)
        {
            int val = 0;
            if (int.TryParse(param, out val))
            {
                return val;
            }

            return 0;
        }

        /// <summary>
        /// parse int for a request form param
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static long GetLongParam(string param)
        {
            long val = 0;
            if (long.TryParse(param, out val))
            {
                return val;
            }

            return 0;
        }

        /// <summary>
        /// parse string for a request form param
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static string GetStringParam(string param)
        {
            if (!string.IsNullOrEmpty(param))
            {
                return param.Trim().ToLower();
            }
            return "";
        }

        /// <summary>
        /// parse bool for a request form param
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static bool GetBoolParam(string param)
        {
            bool val;
            bool.TryParse(param, out val);
            return val;
        }

        /// <summary>
        /// parse nullable bool for a request form param
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static bool? GetNullableBoolParam(string param)
        {
            bool val;
            if (bool.TryParse(param, out val))
            {
                return val;
            }

            return null;
        }

        /// <summary>
        /// parse nullable DateTime for a request form param
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static DateTime? GetNullableDate(string param)
        {
            DateTime val;
            if (DateTime.TryParse(param, out val))
            {
                return val;
            }

            return null;
        }

        /// <summary>
        /// calculate the amount of pages in resultset
        /// </summary>
        /// <param name="count">count of records from query</param>
        /// <param name="limit">record limit per page</param>
        /// <returns></returns>
        public static decimal TotalPages(int count, int limit)
        {
            return count > 0 ? Math.Ceiling(Convert.ToDecimal(count) / Convert.ToDecimal(limit)) : 0m;
        }

        /// <summary>
        /// calculate the requested page from client
        /// </summary>
        /// <param name="page"></param>
        /// <param name="totalPages"></param>
        /// <returns></returns>
        public static int Page(int page, decimal totalPages)
        {
            return page > totalPages ? Convert.ToInt32(totalPages) : page;
        }
        /// <summary>
        /// calculate starting point of requested page
        /// </summary>
        /// <returns></returns>
        public static int Start(int PageSize, int PageIndex)
        {
            return PageSize > 0 && PageIndex > 0 ? (PageIndex * PageSize) - PageSize : 0;
        }
    }
}
