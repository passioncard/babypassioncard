﻿using PassionCardAdmin.Models;
using PassionCardAdmin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static PassionCardAdmin.Enums.Enum;
using PassionCardAdmin.Helpers;
using PassionCardAdmin.Utilities;

namespace PassionCardAdmin.Helpers
{
    public class ImportExportFile
    {
        public long InsertRecordCount { get; internal set; }
        private Notify notify;
        public int RecordID { get; set; }

        internal bool ImportFile(string filePath, string txtFileType)
        {
            try
            {
                if (filePath == null)
                    return false;

                int fileLineCounter = 0;
                string line;
                List<string> lineDataSting = new List<string>();
                var fileName = System.IO.Path.GetFileName(filePath);

                System.IO.StreamReader file = new System.IO.StreamReader(filePath);
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Length > 0)
                    {
                        lineDataSting.Add(line);
                        fileLineCounter++;
                    }
                }
                file.Close();

                //Minimum 2 lines required for validate header and footer
                //If 1 or less than 1 lines in file then return
                if (lineDataSting.Count <= 1)
                    return false;

                var fileMasterModel = new FileMasterModel();

                string header = lineDataSting[0];
                if (header.Length >= 15)
                {
                    fileMasterModel.HeaderFileID = header.Substring(0, 7);
                    string year = string.Empty, month = string.Empty, day = string.Empty;
                    Int32 yearVal = 0, monthVal = 0, dayVal = 0;
                    Int32.TryParse(header.Substring(7, 4), out yearVal);
                    Int32.TryParse(header.Substring(11, 2), out monthVal);
                    Int32.TryParse(header.Substring(13, 2), out dayVal);
                    try
                    {
                        fileMasterModel.HeaderFileCreationDate = new DateTime(yearVal, monthVal, dayVal);
                    }
                    catch (Exception) { }
                }

                string footer = lineDataSting[lineDataSting.Count - 1];
                if (footer.Length >= 16)
                {
                    fileMasterModel.FooterFileID = footer.Substring(0, 8);
                    int footerCount = 0;
                    int.TryParse(footer.Substring(8, 8), out footerCount);
                    fileMasterModel.FooterRecordTotals = footerCount;
                }
                fileMasterModel.FileName = fileName;
                fileMasterModel.RecordCreatedDate = DateTime.Now;
                fileMasterModel.FileType = (int)FileTypes.Import;
                fileMasterModel.UserID = ConstantVar.CurrentUserId;
                var fileService = new FileMasterService();
                int fileRecordId = 0;
                using (PCAEntities db = new PCAEntities())
                {
                    using (var dataContrextTrans = db.Database.BeginTransaction())
                    {
                        try
                        {
                            var fileMasterDbModel = fileService.GetFileMasterRecord(fileName);
                            if (fileMasterDbModel == null)
                            {
                                var ob = fileService.SaveOrUpdateFileDetail(fileMasterModel, db);
                                if (db.tblFileMasters.Count() > 0)
                                    fileRecordId = db.tblFileMasters.Max(x => x.RecordID) + 1;
                                else
                                    fileRecordId = 1;
                            }
                            //fileRecordId = db.tblFileMasters.Max(x => x.RecordID);
                            else
                            {
                                fileRecordId = fileMasterDbModel.RecordID;
                            }
                            List<CardMasterModel> modelList = new List<CardMasterModel>();
                            for (int i = 1; i < lineDataSting.Count - 1; i++)
                            {
                                var obj = ParseCardMasterModel(lineDataSting[i], txtFileType);

                                //obj.FileMasterId
                                obj.RecordCreatedDt = DateTime.Now;
                                obj.RecordModifiedDt = DateTime.Now;
                                obj.FileMasterId = fileRecordId;
                                obj.UserId = ConstantVar.CurrentUserId;
                                modelList.Add(obj);
                            }
                            var cardServiceObj = new CardMasterService();
                            var result = cardServiceObj.SaveCardMasterList(modelList, db);
                            if (result)
                            {
                                db.SaveChanges();
                                dataContrextTrans.Commit();
                                RecordID = fileRecordId;
                                return true;
                            }
                        }
                        catch (Exception ex)
                        {
                            dataContrextTrans.Rollback();
                            ExceptionHelper.ExceptionTrack(ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                notify = new Notify(NotifyLevel.Error, ex.Message);
                ExceptionHelper.ExceptionTrack(ex);
            }
            return false;
        }

        private CardMasterModel ParseCardMasterModel(string modelString, string txtFileType)
        {
            try
            {
                int offset = 0;
                var newCardModel = new CardMasterModel();

                newCardModel.ActionFlags = modelString.Substring(0, 1).ToTrim();
                newCardModel.Name = modelString.Substring(1, 66).ToTrim();
                if (txtFileType == FileType.PACARD.ToString())//Nilesh Added this on 3-Mar-2016 
                {
                    newCardModel.NameOnCard = modelString.Substring(66, 34).ToTrim();
                    offset = 0;
                }
                else
                {
                    newCardModel.NameOnCard = modelString.Substring(66, 50).ToTrim();
                    offset = 16;
                }
                newCardModel.IDType = modelString.Substring((offset + 101), 3).ToTrim();
                newCardModel.IDNumber = modelString.Substring((offset + 104), 20).ToTrim();
                decimal d = 0;
                decimal.TryParse(modelString.Substring((offset + 124), 16).ToTrim(), out d);
                newCardModel.PAssionCardNumber = d;
                newCardModel.PreferredBU = modelString.Substring((offset + 140), 6).ToTrim();
                newCardModel.EmailAddress = modelString.Substring((offset + 146), 40).ToTrim();
                newCardModel.MA_BlockHouseNo = modelString.Substring((offset + 186), 15).ToTrim();
                newCardModel.MA_StreetName = modelString.Substring((offset + 201), 75).ToTrim();
                newCardModel.MA_FloorNo = modelString.Substring((offset + 276), 35).ToTrim();
                newCardModel.MA_UnitNo = modelString.Substring((offset + 311), 35).ToTrim();
                newCardModel.MA_BuildingName = modelString.Substring((offset + 346), 80).ToTrim();
                newCardModel.MA_PostalCode = modelString.Substring((offset + 426), 8).ToTrim();
                newCardModel.HA_BlockHouseNo = modelString.Substring((offset + 434), 15).ToTrim();
                newCardModel.HA_StreetName = modelString.Substring((offset + 449), 75).ToTrim();
                newCardModel.HA_FloorNo = modelString.Substring((offset + 524), 35).ToTrim();
                newCardModel.HA_UnitNo = modelString.Substring((offset + 559), 35).ToTrim();
                newCardModel.HA_BuildingName = modelString.Substring((offset + 594), 80).ToTrim();
                newCardModel.HA_PostalCode = modelString.Substring((offset + 674), 8).ToTrim();
                newCardModel.ExpiryDate = StringToDate(modelString.Substring((offset + 682), 11).ToTrim());
                newCardModel.HpNumber = modelString.Substring((offset + 693), 20).ToTrim();
                newCardModel.ContactNumberHome = modelString.Substring((offset + 713), 15).ToTrim();
                newCardModel.WV = modelString.Substring((offset + 728), 1).ToTrim();
                newCardModel.PAYM = modelString.Substring((offset + 729), 1).ToTrim();
                newCardModel.CoBranded = modelString.Substring((offset + 730), 1).ToTrim();
                newCardModel.CoBrandedCardType = modelString.Substring((offset + 731), 4).ToTrim();
                newCardModel.CardTypeFlag = modelString.Substring((offset + 735), 3).ToTrim();
                newCardModel.Birthday = StringToDate(modelString.Substring((offset + 738), 11).ToTrim());
                newCardModel.Gender = modelString.Substring((offset + 749), 3).ToTrim();
                newCardModel.MaritalStatus = modelString.Substring((offset + 752), 1).ToTrim();
                newCardModel.AnnualIncome = modelString.Substring((offset + 753), 3).ToTrim();
                newCardModel.HousingType = modelString.Substring((offset + 756), 3).ToTrim();
                newCardModel.HighestEducationLevel = modelString.Substring((offset + 759), 3).ToTrim();
                newCardModel.PrintBU = modelString.Substring((offset + 762), 6).ToTrim();
                newCardModel.PreferredWV = modelString.Substring((offset + 768), 6).ToTrim();
                newCardModel.BillingBU = modelString.Substring((offset + 774), 6).ToTrim();
                newCardModel.OldPassionezLinkNumber = modelString.Substring((offset + 780), 16).ToTrim();
                newCardModel.Nationality = modelString.Substring((offset + 796), 2).ToTrim();
                newCardModel.Title = modelString.Substring((offset + 798), 2).ToTrim();
                newCardModel.Race = modelString.Substring((offset + 800), 2).ToTrim();
                newCardModel.Occupation = modelString.Substring((offset + 802), 3);
                newCardModel.CardCANExpiryDt = null;

                return newCardModel;
            }
            catch (Exception ex)
            {
                notify = new Notify(NotifyLevel.Error, ex.Message);
                ExceptionHelper.ExceptionTrack(ex);
            }
            return null;
        }

        /// <summary>
        /// parse date time from string , if less than 11 char retun currnt date
        /// </summary>
        /// <param name="dateStr"></param>
        /// <returns></returns>
        private DateTime StringToDate(string dateStr)
        {
            if (dateStr.Length >= 11)
            {
                DateTime dt1 = DateTime.Now;
                DateTime.TryParse(dateStr, out dt1);
                return dt1;
            }
            else
            {
                //TODO cehck : is it valid to insert current date
                return DateTime.Now;
            }
        }
    }
}