﻿

namespace PassionCardAdmin.Helpers
{
    using System;
    using System.Web;
    using System.Web.Security;

    public static class HashPasswordGenerator
    {
        public static string HashPassword(this string pass)
        {
            var salt = "3uhDS&*asydg3gDHajh32lk;2[okhsfdsaduSGD732";
            return FormsAuthentication.HashPasswordForStoringInConfigFile(pass + salt, "sha1");
        }

        public static object CreateTrackData(int activeUserId)
        {
            return new
            {
                CreatorUserId = activeUserId,
                Created = DateTime.Now,
            };
        }

        public static object ModifyTrackData(int activeUserId)
        {
            return new
            {
                ModifierUserId = activeUserId,
                Modified = DateTime.Now,
            };
        }

        public static string BaseUrl(HttpRequestBase Request)
        {
            var url = Request.Url.Scheme +
                    "://" +
                    Request.Url.Authority +
                    Request.ApplicationPath.TrimEnd('/') +
                    '/';

            return url;
        }

        internal static string RandomPassword(int length = 8)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-";
            char[] chars = new char[length];
            Random rd = new Random();

            for (int i = 0; i < length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }
    }
}