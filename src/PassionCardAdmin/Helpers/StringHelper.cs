﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace PassionCardAdmin.Helpers
{
    public static class StringHelper
    {
        public static T ToEnum<T>(this string enumString)
        {
            return (T)Enum.Parse(typeof(T), enumString);
        }

        /// <summary>
        /// return empty string 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToTrim(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;
            else
                return str.Trim();
        }


        public static string ToPMembership(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;
            else
            {
                StringBuilder returnArray = new StringBuilder();
                var strArray = str.ToCharArray();
                for (int i = 1; i <= strArray.Length; i++)
                {
                    returnArray.Append(strArray[i - 1]);
                    if (i % 4 == 0)
                        returnArray.Append(" ");
                }
                return returnArray.ToString().Trim();
            }
        }

        public static string IsStringNullCHeck(this string str)
        {
            if (string.IsNullOrEmpty(str))
                return "";
            else
            {
                return str.Trim();
            }
        }
    }
}