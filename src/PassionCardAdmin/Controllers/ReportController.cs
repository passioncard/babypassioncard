﻿using PassionCardAdmin.Helpers;
using PassionCardAdmin.Models;
using PassionCardAdmin.Services;
using PassionCardAdmin.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PassionCardAdmin.Controllers
{
    public class ReportController : Controller
    {
        private FileService fileService;
        private RecordService recordService;
        private PhotoReportService photoReportService;
        protected Notify notify { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        public ReportController()
        {
            fileService = new FileService();
            recordService = new RecordService();
            photoReportService = new PhotoReportService();
        }

        // GET: Report
        [HttpGet]
        public ActionResult FileReport()
        {
            return View();
        }

        [HttpGet]
        public ActionResult RecordReport()
        {
            return View();
        }

        /// <summary>
        /// Summary Report View.
        /// </summary>
        /// <returns></returns>
        public ActionResult SummaryReport()
        {
            return View();
        }

        /// <summary>
        /// Print BU Report.
        /// </summary>
        /// <returns></returns>
        public ActionResult PrintBUReport()
        {
            return View();
        }

        /// <summary>
        /// Print BU Report.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PrintBUReport(PrintBUReportModel model)
        {
            var data = recordService.GetPrintBUReport(model);
            if (data.Count > 0)
            {
                recordService.ExportPrintBUReport(data, model, false, Response, Server);
            }
            else
            {
                notify = new Notify(NotifyLevel.Warning, "No records available for export.");
            }
            return View(model);
        }

        //Download file on download link given in list.
        public ActionResult Download(int RecordId, int FileType, string FileName)
        {
            var localpath = ConfigurationManager.AppSettings["ServerPathToSaveFiles"].ToString();
            var extension = GetExtensionByType(FileType);
            FileName = GetFileNameByType(FileType, FileName);

            var localfilename = Path.Combine(localpath, RecordId + extension);

            if (System.IO.File.Exists(localfilename))
            {
                return File(localfilename, "multipart/form-data", FileName);
            }
            else
            {
                notify = new Notify(NotifyLevel.Error, "File does not exists.");
                return RedirectToAction("FileReport");
            }
        }

        public ActionResult EditRecord(int RecordId)
        {
            var record = fileService.GetRecord(RecordId);
            if (record != null)
            {
                if (record.RecordStatus == Enums.Enum.RecordStatus.PC.ToString())
                {
                    CardEditModel model = new CardEditModel()
                    {
                        RecordId = record.RecordID,
                        CardCANExpiryDt = record.CardCANExpiryDt,
                        CardCANNumber = record.CardCANNumber
                    };
                    return View(model);
                }
                else
                {
                    notify = new Notify(NotifyLevel.Error, "Record status must be PC or RD");
                    return RedirectToAction("RecordReport");
                }
            }
            else
            {
                notify = new Notify(NotifyLevel.Error, "Record not found.");
                return RedirectToAction("RecordReport");
            }
        }

        [HttpPost]
        public ActionResult EditRecord(CardEditModel model)
        {
            if (ModelState.IsValid)
            {
                var result = fileService.UpdateCardDetails(model);
                if (result)
                {
                    notify = new Notify(NotifyLevel.Success, "Card details are updated successfully.");
                    return RedirectToAction("RecordReport");
                }
                else
                {
                    ModelState.AddModelError("", "Something went wrong while updating record");
                }
            }
            return View(model);
        }

        /// <summary>
        /// This function Shows report for reset photo images
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PhotoResetReport()
        {
            return View();
        }

        /// <summary>
        /// Post event for photo reset option.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PhotoResetReport(PhotoResetReportModel model)
        {
            //if (ModelState.IsValid)
            //{
                var result = photoReportService.ExportToReport(model, false, Response, Server);
                if (result)
                {

                }
            //}
            return View(model);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (ConstantVar.IsPasswordExpired)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Account",
                    action = "ChangePassword"
                }));
            }
            base.OnActionExecuting(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (notify != null)
                TempData["notify"] = notify;

            base.OnActionExecuted(filterContext);
        }

        #region Helper Methods

        private string GetExtensionByType(int fileType)
        {
            if (fileType == (int)Enums.Enum.FileTypes.ExcelExport)
                return ".zip";
            else
                return ".txt";
        }

        private string GetFileNameByType(int fileType, string FileName)
        {
            if (fileType == (int)Enums.Enum.FileTypes.ExcelExport)
                return FileName + ".zip";
            else
                return FileName;
        }

        /// <summary>
        /// Get File list as per filter applied on view.
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public JsonResult GetFileReport(FormCollection form)
        {
            return Json(fileService.GetFileList(form), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get card details list as per filter applied on view.
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public JsonResult GetCardMasterDetailsReport(FormCollection form)
        {
            return Json(recordService.GetRecordList(form), JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Get data by type(monthly/weekly)
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public JsonResult GetDataByType(FormCollection form)
        {
            return Json(recordService.GetDataByType(form), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get data of printBU Report
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public JsonResult GetPrintBUReport(FormCollection form)
        {
            return Json(recordService.GetPrintBUReport(form), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get all data from Image gistory table.
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public JsonResult GetImageReport(FormCollection form)
        {
            return Json(photoReportService.GetPhotoReport(form), JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}