﻿using PassionCardAdmin.Helpers;
using PassionCardAdmin.Models;
using PassionCardAdmin.Services;
using PassionCardAdmin.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PassionCardAdmin.Controllers
{
    public class CanTapController : Controller
    {
        private Notify notify;
        private CardTapService _cardTapService;

        public CanTapController()
        {
            _cardTapService = new CardTapService();
        }

        // GET: CanTap
        public ActionResult Index()
        {
            try
            {
                var cardService = new CardMasterService();
                _cardTapService = new CardTapService();
                //var list = cardService.GetTappingList();
                //ViewBag.TappingList = list;
                var ImageFlag = ConfigurationManager.AppSettings["LoadImagesOnCardTapScreen"].ToString();
                ViewBag.ImageFlag = ImageFlag;
                ViewBag.PrintBuList = cardService.GetPrintBUList();

                //if (list.Count > 0)
                //{
                //    ViewBag.Gender = list[0].GenderStr;
                //    ViewBag.IDNumber = list[0].IDNumber;
                //}
                //else
                //{
                //    ViewBag.Gender = string.Empty;
                //    ViewBag.IDNumber = string.Empty;
                //}
            }
            catch (Exception ex)
            {
                notify = new Notify(NotifyLevel.Error, ex.Message);
                ExceptionHelper.ExceptionTrack(ex);
            }

            //return View(((List<CardTappingModel>)ViewBag.TappingList).FirstOrDefault());
            return View();
        }

        [HttpPost]
        public ActionResult Index(CardTappingModel model)
        {            
            return View();
        }

        [HttpPost]
        public JsonResult SaveTapping(FormCollection form)
        {
            try
            {
                var cardService = new CardMasterService();
                var PAssionCardNumber = _cardTapService.GetStringFromForm(form, "PAssionCardNumber");
                var CardCANNumber = _cardTapService.GetStringFromForm(form, "CardCANNumber");
                var CardCANExpiryDt = _cardTapService.GetStringFromForm(form, "CardCANExpiryDt");
                var idnumber = _cardTapService.GetStringFromForm(form, "idnumber");

                if (string.IsNullOrEmpty(CardCANNumber) || string.IsNullOrEmpty(CardCANExpiryDt) || string.IsNullOrEmpty(PAssionCardNumber) || string.IsNullOrEmpty(idnumber))
                {
                    return Json(new CardTapListPagingModel(), JsonRequestBehavior.AllowGet);
                }
                if (CardCANNumber.Length == 16 && CardCANExpiryDt.Length == 4)
                {
                    cardService.SaveTappingDetails(idnumber, PAssionCardNumber, CardCANNumber, CardCANExpiryDt);
                    return GetTapControllerData(form);
                }
                else
                {
                    notify = new Notify(NotifyLevel.Error, "Invalid data.");
                    return null;
                }
            }
            catch (Exception ex)
            {
                notify = new Notify(NotifyLevel.Error, ex.Message);
                ExceptionHelper.ExceptionTrack(ex);
                return null;
            }
        }

        public ActionResult GetImage(string img)
        {
            try
            {
                var cardService = new CardMasterService();
                byte[] data = cardService.getImageData(img);
                //var test = Convert.ToBase64String(data);
                //return Json(new { base64imgage = Convert.ToBase64String(data) }, JsonRequestBehavior.AllowGet);
                var jsonResult = Json(new { base64imgage = Convert.ToBase64String(data) }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                notify = new Notify(NotifyLevel.Error, ex.Message);
                ExceptionHelper.ExceptionTrack(ex);
                return null;
            }

        }

        public JsonResult GetTapControllerData(FormCollection form)
        {
            return Json(_cardTapService.GetCardDetalsList(form), JsonRequestBehavior.AllowGet);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (ConstantVar.IsPasswordExpired)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Account",
                    action = "ChangePassword"
                }));
            }
            base.OnActionExecuting(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (notify != null)
                TempData["notify"] = notify;

            base.OnActionExecuted(filterContext);
        }
    }
}