﻿using PassionCardAdmin.AuthService;
using PassionCardAdmin.Helpers;
using PassionCardAdmin.Models;
using PassionCardAdmin.Services;
using PassionCardAdmin.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using static PassionCardAdmin.Enums.Enum;

namespace PassionCardAdmin.Controllers
{
    [Authorize(Roles = ConstantVar.AdminUserRoles)]
    public class UserController : Controller
    {
        private UserService userService;
        private AccountService accountService;
        private IAuthenticationService auth;
        private Notify notify;

        /// <summary>
        /// Constructor
        /// </summary>
        public UserController()
        {
            userService = new UserService();
            accountService = new AccountService();
            auth = new FormsAuthenticationService();
        }


        // GET: User
        public ActionResult UserList()
        {
            return View();
        }

        /// <summary>
        /// Create new User
        /// </summary>
        /// <returns></returns>
        public ActionResult CreateUser()
        {
            ViewBag.IsEdit = false;
            ViewBag.UserTypeList = GetUserTypes();
            return View();
        }

        private List<UserTypeModel> GetUserTypes()
        {
            List<UserTypeModel> list = new List<UserTypeModel>();
            list.Add(new UserTypeModel { Text = UserRole.Admin.ToString(), Value = UserRole.Admin });
            list.Add(new UserTypeModel { Text = UserRole.User.ToString(), Value = UserRole.User });
            return list;
        }

        /// <summary>
        /// Create new User
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateUser(CreateUserModel model)
        {
            ViewBag.IsEdit = false;
            ViewBag.UserTypeList = GetUserTypes();
            if (ModelState.IsValid)
            {
                if (!userService.CheckUserName(model.UserName))
                {
                    var result = userService.AddUser(model);
                    if (result)
                    {
                        notify = new Notify(NotifyLevel.Success, "User created successfully.");
                        return RedirectToAction("UserList");
                    }
                }
                else
                {
                    ModelState.AddModelError("UserName", "An user with same Username already exists in the system.");
                }
            }
            return View(model);
        }

        /// <summary>
        /// Create new User
        /// </summary>
        /// <returns></returns>
        public ActionResult EditUser(int? UserId, bool IsEdit)
        {
            try
            {
                ViewBag.IsEdit = true;

                if (IsEdit)
                    ViewBag.EditType = "Edit";
                else
                    ViewBag.EditType = "ChangePassword";
                if (UserId.HasValue)
                {
                    var user = userService.GetCurrentUser(UserId.Value);
                    if (user != null)
                    {
                        CreateUserModel model = new CreateUserModel();
                        model = model.CopyFromUserModel(user);
                        model.IsEdit = IsEdit;
                        return View(model);
                    }
                    else
                    {
                        notify = new Notify(NotifyLevel.Error, "Requested user not found.");
                    }
                }
                else
                {
                    return RedirectToAction("UserList");
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
            }
            return RedirectToAction("UserList");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditUser(CreateUserModel model)
        {
            ViewBag.IsEdit = true;
            try
            {
                bool flag = false;
                if (!ModelState.IsValid)
                {
                    if (model.IsEdit)
                    {
                        var USERNAME = ModelState.Keys.ToList().IndexOf("UserName");
                        var Name = ModelState.Keys.ToList().IndexOf("Name");
                        if (ModelState.Values.ToList()[USERNAME].Errors.Count() > 0 || ModelState.Values.ToList()[Name].Errors.Count() > 0)
                        {
                            flag = false;
                        }
                        else
                        {
                            flag = true;
                        }
                    }
                    else
                    {
                        var Password = ModelState.Keys.ToList().IndexOf("Password");
                        var ConfirmPassword = ModelState.Keys.ToList().IndexOf("ConfirmPassword");
                        if (ModelState.Values.ToList()[Password].Errors.Count() > 0 || ModelState.Values.ToList()[ConfirmPassword].Errors.Count() > 0)
                        {
                            flag = false;
                        }
                        else
                        {
                            flag = true;
                        }
                    }
                    if (model.IsEdit)
                        ViewBag.EditType = "Edit";
                    else
                        ViewBag.EditType = "ChangePassword";

                    if (flag)
                    {
                        var user = userService.GetCurrentUser(model.UserId);
                        if (user != null)
                        {
                            var result = userService.UpdateUser(model);
                            if (result)
                            {
                                var userModel = accountService.GetValidUser(ConstantVar.CurrentUserName);
                                auth.Login(ConstantVar.GetINameString(userModel), userModel.RoleStr);
                                notify = new Notify(NotifyLevel.Success, "User updated successfully.");
                                return RedirectToAction("UserList");
                            }
                            else
                            {
                                foreach (var error in userService.Errors)
                                {
                                    ModelState.AddModelError(error.Key, error.Value);
                                }
                                return View(model);
                            }
                        }
                        else
                        {
                            notify = new Notify(NotifyLevel.Error, "Requested user not found.");
                            return View(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
            }
            return View(model);
        }

        ///<summary>
        ///Unblock User
        ///</summary>
        public JsonResult UnblockUser(FormCollection form)
        {
            return Json(userService.UnblockUser(form), JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Activate Deactivate user account
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>       
        public JsonResult Activate(FormCollection form)
        {
            return Json(userService.ActivateDeacivatUser(form), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get User list as per filter applied on view.
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public JsonResult GetUserList(FormCollection form)
        {
            return Json(userService.GetUserList(form), JsonRequestBehavior.AllowGet);
        }

        #region "Helper methods"

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (ConstantVar.IsPasswordExpired)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Account",
                    action = "ChangePassword"
                }));
            }
            base.OnActionExecuting(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (notify != null)
                TempData["notify"] = notify;

            base.OnActionExecuted(filterContext);
        }

        #endregion
    }
}