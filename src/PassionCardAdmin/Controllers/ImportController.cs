﻿using PassionCardAdmin.Helpers;
using PassionCardAdmin.Models;
using PassionCardAdmin.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using static PassionCardAdmin.Enums.Enum;

namespace PassionCardAdmin.Controllers
{
    public class ImportController : Controller
    {
        protected Notify notify { get; set; }

        // GET: Import
        public ActionResult Index()
        {
            ViewBag.FileTypeList = GetTypeList();
            return View();
        }

        [HttpPost]
        public ActionResult Index(System.Web.HttpPostedFileBase importFile, string txtFileType)
        {
            try
            {
                ViewBag.FileTypeList = GetTypeList();
                if (importFile != null && importFile.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(importFile.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/ImportTXTFile/"), fileName);
                    if (!Directory.Exists(Server.MapPath("~/Content/ImportTXTFile/")))
                        Directory.CreateDirectory(Server.MapPath("~/Content/ImportTXTFile/"));

                    importFile.SaveAs(path);

                    if (ValidateFileWithType(txtFileType, Server.MapPath("~/Content/ImportTXTFile/") + fileName))
                    {
                        ImportExportFile ief = new ImportExportFile();
                        var result = ief.ImportFile(Server.MapPath("~/Content/ImportTXTFile/") + fileName, txtFileType);
                        if (result)
                        {
                            #region SaveToServerPath
                            if (ief.RecordID > 0)
                            {
                                var localpath = ConfigurationManager.AppSettings["ServerPathToSaveFiles"].ToString();
                                if (!Directory.Exists(localpath)) Directory.CreateDirectory(localpath);
                                var localfilename = Path.Combine(localpath, ief.RecordID + ".txt");

                                System.IO.File.WriteAllBytes(localfilename, System.IO.File.ReadAllBytes(path));
                            }
                            #endregion

                            notify = new Notify(NotifyLevel.Success, "Data Imported Successfully.");
                            return RedirectToAction("Index", "Import", new { ImportCount = ief.InsertRecordCount });
                        }
                        else
                        {
                            notify = new Notify(NotifyLevel.Error, "Data Import unsuccessful.");
                        }
                    }
                    else
                    {
                        notify = new Notify(NotifyLevel.Info, "File type mismatched.");
                    }
                }
                else
                {
                    notify = new Notify(NotifyLevel.Info, "Please select file to Import.");
                }
            }
            catch (Exception ex)
            {
                notify = new Notify(NotifyLevel.Error, ex.Message);
                ExceptionHelper.ExceptionTrack(ex);
            }
            return RedirectToAction("Index", "Import");
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (ConstantVar.IsPasswordExpired)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Account",
                    action = "ChangePassword"
                }));
            }
            base.OnActionExecuting(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (notify != null)
                TempData["notify"] = notify;

            base.OnActionExecuted(filterContext);
        }

        #region "Helper methods"

        private List<FileTypeModel> GetTypeList()
        {
            List<FileTypeModel> list = new List<FileTypeModel>();
            list.Add(new FileTypeModel() { Text = FileType.PACARD.ToString(), Value = FileType.PACARD });
            list.Add(new FileTypeModel() { Text = FileType.SACARD.ToString(), Value = FileType.SACARD });

            return list;
        }

        /// <summary>
        /// Validate file with appropriate type.
        /// </summary>
        /// <param name="txtFileType"></param>
        /// <param name="importFile"></param>
        /// <returns></returns>
        private bool ValidateFileWithType(string txtFileType, string importFile)
        {
            bool value = false;
            if (importFile == null)
                return false;

            string line;
            List<string> lineDataSting = new List<string>();
            var fileName = System.IO.Path.GetFileName(importFile);

            System.IO.StreamReader file = new System.IO.StreamReader(importFile);
            int lineNo = 0;
            while ((line = file.ReadLine()) != null)
            {
                if (line.Length > 0)
                {
                    if (lineNo == 1)
                    {
                        if (line.Length == PassionCardAdmin.Utilities.ConstantVar.PACARDLength && txtFileType == FileType.PACARD.ToString())
                        {
                            value = true;
                            break;
                        }
                        else if (line.Length == PassionCardAdmin.Utilities.ConstantVar.SACARDLength && txtFileType == FileType.SACARD.ToString())
                        {
                            value = true;
                            break;
                        }
                        else
                        {
                            value = false;
                            break;
                        }
                    }
                    lineNo++;
                }
            }
            file.Close();
            return value;
        }

        #endregion
    }
}