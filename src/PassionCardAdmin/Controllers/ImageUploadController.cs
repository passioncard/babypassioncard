﻿using PassionCardAdmin.Helpers;
using PassionCardAdmin.Models;
using PassionCardAdmin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PassionCardAdmin.Controllers
{
    public class ImageUploadController : Controller
    {
        private PhotoUploadService _PhotoUploadService;

        protected Notify notify { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public ImageUploadController()
        {
            _PhotoUploadService = new PhotoUploadService();
        }


        // GET: ImageUpload
        [HttpGet]
        public ActionResult ResetImage()
        {
            return View();
        }

        /// <summary>
        /// http post for Reset Image Event
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ResetImage(PhotoResetModel model)
        {
            if (ModelState.IsValid)
            {
                var DatabaseModel = _PhotoUploadService.GetData(model.ICNumber);
                if (DatabaseModel != null)
                {
                    if (DatabaseModel.PictureUpdated == false)
                    {
                        ModelState.AddModelError("", "photo not uploaded for this record");
                        return View(model);
                    }
                    else
                    {
                        var result = _PhotoUploadService.ResetPhotoStatus(model.ICNumber);
                        notify = new Notify(NotifyLevel.Success,"Photo status updated successfully");
                        model = new PhotoResetModel();
                        model.isShowMessage = true;
                        return View(model);
                    }
                }
            }
            model.isShowMessage = false;
            return View(model);
        }

        /// <summary>
        /// Return Data as per IcNumber mentioned in form
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public JsonResult GetICNumberData(FormCollection formParams)
        {
            var form = new GenericListParams(formParams);
            var txtICNumber = ParamsHelper.GetStringParam(form.Params["ICNumber"]);
            var jsonResult = Json(new { data = _PhotoUploadService.GetData(txtICNumber), img = GetImage(txtICNumber) }, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        /// <summary>
        /// Return Image base64 image of ICNumber
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public object GetImage(string img)
        {
            try
            {
                var cardService = new CardMasterService();
                byte[] data = cardService.getImageData(img);
                return Convert.ToBase64String(data);
                //var jsonResult = Json(new { base64imgage = Convert.ToBase64String(data) }, JsonRequestBehavior.AllowGet);
                //jsonResult.MaxJsonLength = int.MaxValue;
                //return jsonResult;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
    }
}