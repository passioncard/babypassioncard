﻿using Microsoft.Reporting.WebForms;
using PassionCardAdmin.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PassionCardAdmin.Models;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using System.IO;
using PassionCardAdmin.Services;
using PassionCardAdmin.Utilities;
using System.Web.Routing;

namespace PassionCardAdmin.Controllers
{
    public class ExportController : Controller
    {
        protected CardMasterService cardMasterService;
        protected Notify notify { get; set; }

        public ExportController()
        {
            cardMasterService = new CardMasterService();
        }

        // GET: Export
        public ActionResult Index()
        {
            ExportModelList model = new ExportModelList();
            model.MainExportModel = ConvertToModel(cardMasterService.GetExcelExportList());
            return View(model);
        }

        [HttpPost]
        public ActionResult ExportExcel(ExportModelList model)
        {
            try
            {
                var cardMasterService = new PassionCardAdmin.Services.CardMasterService();
                var dataList = cardMasterService.GetExcelExportList(model);
                var list = new List<ExportToXlsModel>();
                var groupedData = (from record in dataList
                                   group record by record.PrintBUStr into data
                                   orderby data.Key
                                   select new
                                   {
                                       GroupKey = data.Key,
                                       GroupValuesList = data.ToList()
                                   });
                //dataList.Clear();
                foreach (var group in groupedData)
                {
                    if (group.GroupValuesList.Count() > 0)
                    {
                        foreach (var item in group.GroupValuesList)
                        {
                            list.Add(item);
                        }

                    }
                }

                if (list != null && list.Count > 0)
                {
                    LocalReport localReport = new LocalReport();

                    localReport.ReportPath = Server.MapPath(Utilities.ConstantVar.ReportPath + "PA-Baby.rdlc");

                    ReportDataSource reportDataSource = new ReportDataSource("DataSet1", list);
                    localReport.DataSources.Clear();
                    localReport.DataSources.Add(reportDataSource);

                    localReport.Refresh();

                    string extensionString = "xls";
                    string outputType = "Excel";

                    string opFileName = "printing_list_PA_Baby_" + DateTime.Now.ToString("yyyyMMdd");

                    int recordID = cardMasterService.SavePrintFileDetails(opFileName, list);
                    if (recordID > 0)
                    {
                        ExportReportHelper.ReportWithImagesZip(localReport, outputType, opFileName, true, extensionString, Response, list, recordID);
                        ExportModelList returnModel = new ExportModelList();
                        returnModel.MainExportModel = ConvertToModel(cardMasterService.GetExcelExportList());
                        //return View(returnModel);
                        return RedirectToAction("Index");
                    }
                    else
                        notify = new Notify(NotifyLevel.Error, "Unable to update records.");
                }
                else
                {
                    notify = new Notify(NotifyLevel.Warning, "No records available for printing.");
                }
            }
            catch (Exception ex)
            {
                notify = new Notify(NotifyLevel.Error, ex.Message);
                ExceptionHelper.ExceptionTrack(ex);
            }
            return RedirectToAction("Index");
        }



        public ActionResult ACK()
        {
            ExportModelList model = new ExportModelList();
            model.MainExportModel = ConvertToModel(cardMasterService.GetAckExportList());
            return View(model);
        }

        [HttpPost]
        public ActionResult Ack(DateTime? ackDate, ExportModelList model)
        {
            try
            {
                if (ackDate == null)
                {
                    notify = new Notify(NotifyLevel.Error, "Please select date.");
                    model.MainExportModel.ForEach(x => x.IsChecked = true);
                    return View(model);
                }

                var cardMasterService = new PassionCardAdmin.Services.CardMasterService();
                var list = cardMasterService.GetAckExportList(model);

                if (list != null && list.Count > 0)
                {
                    List<string> lineList = new List<string>();
                    lineList.Add("BBCARDACK" + ackDate.Value.ToString("yyyyMMdd"));
                    foreach (var crdRecord in list)
                    {
                        lineList.Add(crdRecord.PAssionCardNumberStr + crdRecord.CanNumberStr + crdRecord.ExpiryDtYYMM + crdRecord.ActionFlags + GetReturnCode("0") + GetReturnMessage("") + GetOldezLinkNumber(crdRecord.OldPassionezLinkNumber));
                    }
                    lineList.Add("99999999" + list.Count.ToString("00000000"));
                    var fileName = "BBCARD_ACK_" + ackDate.Value.ToString("yyMMdd") + ".txt";

                    StringBuilder str = new StringBuilder();
                    const char LF = '\n';
                    for (int i = 0; i < lineList.Count; i++)
                    {
                        if (i == 0)
                        {
                            str.Append(lineList[i] + LF);
                        }
                        else if (i == (lineList.Count - 1))
                        {
                            str.Append(lineList[(lineList.Count - 1)] + LF);
                        }
                        else
                        {
                            str.Append(lineList[i] + LF);
                        }
                    }

                    var recordId = cardMasterService.SaveAckFileSendDetails(fileName, list);
                    string localfilename = string.Empty;

                    #region SaveToServerPath
                    if (recordId > 0)
                    {
                        var localpath = ConfigurationManager.AppSettings["ServerPathToSaveFiles"].ToString();
                        if (!Directory.Exists(localpath)) Directory.CreateDirectory(localpath);
                        localfilename = Path.Combine(localpath, recordId + ".txt");
                        using (var streamWrite = new StreamWriter(localfilename, true))
                        {
                            streamWrite.Write(str.ToString());
                            streamWrite.Close();
                        }
                    }
                    #endregion

                    return File(localfilename, "multipart/form-data", fileName);
                }
                else
                {
                    notify = new Notify(NotifyLevel.Warning, "No records available for printing.");
                    model.MainExportModel.ForEach(x => x.IsChecked = true);
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                notify = new Notify(NotifyLevel.Error, ex.Message);
                ExceptionHelper.ExceptionTrack(ex);
            }
            return View();
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (ConstantVar.IsPasswordExpired)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Account",
                    action = "ChangePassword"
                }));
            }
            base.OnActionExecuting(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (notify != null)
                TempData["notify"] = notify;

            base.OnActionExecuted(filterContext);
        }

        #region "Helper methods"

        /// <summary>
        /// Convert model from one to another
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private List<ExportModel> ConvertToModel(List<ExportToXlsModel> list)
        {
            return list.Select(x => new ExportModel()
            {
                RecordID = x.RecordId,
                Gender = x.Gender == "001" ? "Female" : "Male",
                IDNumber = x.IDNumber,
                IsChecked = true,
                NameOnCard = x.NameOnCard,
                PAssionCardNumber = x.PAssionCardNumber,
                SrNum = x.SrNo
            }).ToList();
        }


        /// <summary>
        /// retun 10 char string
        /// </summary>
        /// <param name="codeStr"></param>
        /// <returns></returns>
        private string GetReturnCode(string codeStr)
        {
            if (string.IsNullOrEmpty(codeStr))
                codeStr = string.Empty;
            if (codeStr.Length < 10)
                return codeStr.PadRight(10 - codeStr.Length);
            else
                return codeStr;
        }

        /// <summary>
        /// retun 100 char string
        /// </summary>
        /// <param name="messageStr"></param>
        /// <returns></returns>
        private string GetReturnMessage(string messageStr)
        {
            if (string.IsNullOrEmpty(messageStr))
                messageStr = string.Empty;
            if (messageStr.Length < 101)
                return messageStr.PadRight(101 - messageStr.Length);
            else
                return messageStr;
        }

        /// <summary>
        /// retun 16 char string
        /// </summary>
        /// <param name="oldPassionezLinkNumber"></param>
        /// <returns></returns>
        private string GetOldezLinkNumber(string oldPassionezLinkNumber)
        {
            if (string.IsNullOrEmpty(oldPassionezLinkNumber))
                oldPassionezLinkNumber = string.Empty;
            if (oldPassionezLinkNumber.Length < 16)
                return oldPassionezLinkNumber.PadRight(16 - oldPassionezLinkNumber.Length);
            else
                return oldPassionezLinkNumber;
        }

        #endregion       
    }
}