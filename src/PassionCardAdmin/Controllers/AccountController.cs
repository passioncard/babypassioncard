﻿using PassionCardAdmin.AuthService;
using PassionCardAdmin.Helpers;
using PassionCardAdmin.Models;
using PassionCardAdmin.Services;
using PassionCardAdmin.Utilities;
using System;
using System.Text;
using System.Web.Mvc;

namespace PassionCardAdmin.Controllers
{
    public class AccountController : Controller
    {

        private readonly AccountService accountService;
        private IAuthenticationService auth;
        private Notify notify;

        //Constructor
        public AccountController()
        {
            accountService = new AccountService();
            auth = new FormsAuthenticationService();
        }

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        // GET: Account
        [HttpGet]
        public ActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }


        //Login ActionResult for validating USER
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserMasterModel model, string returnUrl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = accountService.GetValidUser(model.UserName);
                    if (user != null)
                    {
                        if (user.WrongPasswordCount >= ConstantVar.MaxWrongPasswordCount)
                        {
                            ModelState.AddModelError("", "Account is blocked. Please contact Admin for more details.");
                            return View(model);
                        }

                        if (HashPasswordGenerator.HashPassword(model.UserPass) != user.UserPass)
                        {
                            accountService.TryAgainCounter(model.UserName);
                            user.WrongPasswordCount++;

                            if (user.WrongPasswordCount >= ConstantVar.MaxWrongPasswordCount)
                                ModelState.AddModelError("", "Account is blocked. Please try by forgot password.");
                            else
                                ModelState.AddModelError("", "Invalid USERNAME or PASSWORD. Please try again.");
                            return View(model);
                        }

                        if (user.IsDelete)
                        {
                            ModelState.AddModelError("", "Account is deactivated. Please activate and try again.");
                            return View(model);
                        }

                        auth.Login(ConstantVar.GetINameString(user), user.RoleStr);
                        accountService.UpdateSuccessfulLoginDetails(model.UserName);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Invalid USERNAME or PASSWORD. Please try again.");
                    }
                }
            }
            catch (Exception ex)
            {
                notify = new Notify(NotifyLevel.Error, ex.Message);
                ExceptionHelper.ExceptionTrack(ex);
            }
            return View(model);
        }

        //Get for change password 
        [HttpGet]
        public ActionResult ChangePassword()
        {
            UserChangePasswordModel model = new UserChangePasswordModel();

            return View();
        }

        //Post for change password.
        [HttpPost]
        public ActionResult ChangePassword(UserChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.NewPassword == model.ConfirmPassword)
                {
                    if (accountService.ChangePasswordAdd(model, Utilities.ConstantVar.CurrentUserName))
                    {
                        var user = accountService.GetValidUser(ConstantVar.CurrentUserName);
                        auth.Login(ConstantVar.GetINameString(user), user.RoleStr);

                        notify = new Notify(NotifyLevel.Success, "Successfully Password Changed.");
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        foreach (var item in accountService.Errors)
                        {
                            ModelState.AddModelError(item.Key, item.Value);
                        }
                    }
                }
            }
            return View(model);
        }

        //Log out from current session
        public ActionResult Logout()
        {
            auth.Logoff();

            return RedirectToAction("Login", "Account");
        }

        //Get for profile Data.
        [HttpGet]
        public ActionResult ProfileData(string userName)
        {

            UserProfileModel userModel = new UserProfileModel();
            userModel.userModel = accountService.GetValidUser(userName);
            ViewBag.UserType = userModel.userModel.RoleStr;
            return View(userModel);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (notify != null)
                TempData["notify"] = notify;

            base.OnActionExecuted(filterContext);
        }


    }
}