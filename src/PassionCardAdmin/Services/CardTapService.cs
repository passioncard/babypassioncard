﻿using PassionCardAdmin.Helpers;
using PassionCardAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PassionCardAdmin.Services
{
    public class CardTapService : ErrorService
    {
        public CardTapService()
        {

        }

        public CardTapListPagingModel GetCardDetalsList(System.Collections.Specialized.NameValueCollection formParams)
        {
            try
            {
                var form = new GenericListParams(formParams);

                var ICNumber = ParamsHelper.GetStringParam(form.Params["ICNumber"]);
                var PrintBU = ParamsHelper.GetStringParam(form.Params["PrintBU"]);//Nilesh added this filter on 31/07/2017
                var Name = ParamsHelper.GetStringParam(form.Params["Name"]);

                using (var db = new PCAEntities())
                {
                    int count = 0;
                    decimal totalPages = 0;
                    int page = 0;

                    //var query = db.tblCardMasters.AsQueryable();//Nilesh changed this on 18/08/2017 to improve get speed.
                    var query = db.tblCardMasters.AsQueryable()
                                .Select(f => new
                                {
                                    f.PictureUpdated,
                                    f.SendInPrintFile,
                                    f.IDNumber,
                                    f.NameOnCard,
                                    f.PrintBU,
                                    f.RecordStatusUpdateDt,
                                    f.Gender,
                                    f.PAssionCardNumber,
                                    f.RecordID,
                                    f.ExpiryDate
                                });

                    List<CardTappingModel> list = new List<CardTappingModel>();

                    query = query.Where(x => x.PictureUpdated && x.SendInPrintFile == (int)Enums.Enum.SendInPrintFile.Print);

                    if (!string.IsNullOrEmpty(ICNumber))
                    {
                        query = query.Where(x => x.IDNumber.Contains(ICNumber));
                    }

                    if (!string.IsNullOrEmpty(Name))
                    {
                        query = query.Where(x => x.NameOnCard.Contains(Name));
                    }

                    if (!string.IsNullOrEmpty(PrintBU))//Nilesh added this filter on 31/07/2017
                    {
                        if (!PrintBU.Equals("0"))
                        {
                            query = query.Where(x => x.PrintBU == PrintBU);
                        }
                    }

                    count = query.Count();
                    totalPages = ParamsHelper.TotalPages(count, form.Limit);
                    page = ParamsHelper.Page(form.Page, totalPages);

                    var returnList = query.OrderBy(x => x.RecordStatusUpdateDt)
                        //.Skip(form.Start)
                        //.Take(form.Limit)
                        .ToList();

                    var printBUList = db.tblPreferredBUMasters.ToList();
                    foreach (var enty in returnList)
                    {
                        var itm = new CardTappingModel();
                        itm.Gender = enty.Gender;
                        itm.PAssionCardNumber = enty.PAssionCardNumber;
                        itm.RecordID = enty.RecordID;
                        itm.IDNumber = enty.IDNumber;
                        itm.NameOnCard = enty.NameOnCard;
                        itm.PAssionCardNumberStr = enty.PAssionCardNumber.ToString().ToPMembership();
                        if (enty.PrintBU != null)
                        {
                            var selectedBU = printBUList.FirstOrDefault(x => x.Code == enty.PrintBU.Trim());
                            if (selectedBU != null)
                            {
                                itm.PrintBU = selectedBU.PreferredBUValue;
                            }
                        }

                        itm.CardCANExpiryDt = enty.ExpiryDate.ToString("dd MMM yyyy").ToUpper();

                        list.Add(itm);
                    }

                    var groupedData = (from record in list
                                       group record by record.PrintBU into data
                                       orderby data.Key
                                       select new
                                       {
                                           GroupKey = data.Key,
                                           GroupValuesList = data.ToList()
                                       });
                    var rList = new List<CardTappingModel>();

                    foreach (var group in groupedData)
                    {
                        if (group.GroupValuesList.Count() > 0)
                        {
                            foreach (var item in group.GroupValuesList)
                            {
                                rList.Add(item);
                            }
                        }
                    }

                    var d = rList.AsEnumerable()
                                 .Skip(form.Start)
                                 .Take(form.Limit);

                    return new CardTapListPagingModel
                    {
                        page = page,
                        total = totalPages,
                        records = count,
                        data = d.ToList()
                    };
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
                return null;
            }

        }


        public string GetStringFromForm(System.Collections.Specialized.NameValueCollection formParams, string parameter)
        {
            var form = new GenericListParams(formParams);

            return ParamsHelper.GetStringParam(form.Params[parameter]);
        }
    }
}