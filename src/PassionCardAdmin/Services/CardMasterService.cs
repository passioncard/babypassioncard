﻿using PassionCardAdmin.Helpers;
using PassionCardAdmin.Models;
using PassionCardAdmin.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;

namespace PassionCardAdmin.Services
{
    public class CardMasterService
    {



        /// <summary>
        /// save the card master data to the database 
        /// if exist the IDNumber then update the existing record
        /// </summary>
        /// <param name="list"></param>
        public bool SaveCardMasterList(List<CardMasterModel> list, PCAEntities db)
        {
            try
            {
                //using (var db = new PCAEntities())
                {
                    List<tblCardMaster> duplicateRecords = new List<tblCardMaster>();
                    foreach (var itm in list)
                    {
                        var entity = db.tblCardMasters.FirstOrDefault(x => x.IDNumber == itm.IDNumber);
                        var duplicate = duplicateRecords.FirstOrDefault(x => x.IDNumber == itm.IDNumber);
                        if (entity == null && duplicate == null)
                        {
                            var model = new tblCardMaster();
                            db.tblCardMasters.Add(itm.CopyFromModelToDb(itm));
                            duplicateRecords.Add(itm.CopyFromModelToDb(itm));
                            //db.SaveChanges();
                        }
                        else
                        {
                            entity.EmailAddress = itm.EmailAddress.ToTrim();
                            entity.RecordID = entity.RecordID;
                            entity.ActionFlags = itm.ActionFlags.ToTrim();

                            entity.Name = itm.Name.ToTrim();
                            entity.NameOnCard = itm.NameOnCard.ToTrim();
                            entity.IDType = itm.IDType.ToTrim();
                            entity.IDNumber = itm.IDNumber.ToTrim();
                            entity.PAssionCardNumber = itm.PAssionCardNumber;
                            entity.PreferredBU = itm.PreferredBU.ToTrim();
                            entity.EmailAddress = itm.EmailAddress.ToTrim();
                            entity.MA_BlockHouseNo = itm.MA_BlockHouseNo.ToTrim();
                            entity.MA_StreetName = itm.MA_StreetName.ToTrim();
                            entity.MA_FloorNo = itm.MA_FloorNo.ToTrim();
                            entity.MA_UnitNo = itm.MA_UnitNo.ToTrim();
                            entity.MA_BuildingName = itm.MA_BuildingName.ToTrim();
                            entity.MA_PostalCode = itm.MA_PostalCode.ToTrim();
                            entity.HA_BlockHouseNo = itm.HA_BlockHouseNo.ToTrim();
                            entity.HA_StreetName = itm.HA_StreetName.ToTrim();
                            entity.HA_FloorNo = itm.HA_FloorNo.ToTrim();
                            entity.HA_UnitNo = itm.HA_UnitNo.ToTrim();
                            entity.HA_BuildingName = itm.HA_BuildingName.ToTrim();
                            entity.HA_PostalCode = itm.HA_PostalCode.ToTrim();
                            entity.ExpiryDate = itm.ExpiryDate;
                            entity.HpNumber = itm.HpNumber.ToTrim();
                            entity.ContactNumberHome = itm.ContactNumberHome.ToTrim();
                            entity.WV = itm.WV.ToTrim();
                            entity.PAYM = itm.PAYM.ToTrim();
                            entity.CoBranded = itm.CoBranded.ToTrim();
                            entity.CoBrandedCardType = itm.CoBrandedCardType.ToTrim();
                            entity.CardTypeFlag = itm.CardTypeFlag.ToTrim();
                            entity.Birthday = itm.Birthday;
                            entity.Gender = itm.Gender.ToTrim();
                            entity.MaritalStatus = itm.MaritalStatus.ToTrim();
                            entity.AnnualIncome = itm.AnnualIncome.ToTrim();
                            entity.HousingType = itm.HousingType.ToTrim();
                            entity.HighestEducationLevel = itm.HighestEducationLevel.ToTrim();
                            entity.PrintBU = itm.PrintBU.ToTrim();
                            entity.PreferredWV = itm.PreferredWV.ToTrim();
                            entity.BillingBU = itm.BillingBU.ToTrim();
                            entity.OldPassionezLinkNumber = itm.OldPassionezLinkNumber.ToTrim();
                            entity.Nationality = itm.Nationality.ToTrim();
                            entity.Title = itm.Title.ToTrim();
                            entity.Race = itm.Race.ToTrim();
                            entity.Occupation = itm.Occupation.ToTrim();
                            entity.UserId = itm.UserId;
                            entity.FileMasterId = itm.FileMasterId;
                            //Not required to update the create date in edit mode, only update modify date
                            //entity.RecordCreatedDt = itm.RecordCreatedDt;
                            entity.RecordModifiedDt = itm.RecordModifiedDt;

                            //for edit as per discussion on 27 Feb 2017 : change the flags, and values
                            entity.SendInPrintFile = (int)Enums.Enum.SendInPrintFile.No;
                            entity.PrintFileRefId = 0;
                            entity.CardCANNumber = 0;
                            entity.CardCANExpiryDt = "";
                            entity.SendAck = false;
                            entity.AckFileRefId = 0;
                            if (entity.RecordStatus.Trim().ToString() != (Enums.Enum.RecordStatus.N).ToString())
                                entity.RecordStatus = Enums.Enum.RecordStatus.PR.ToString();
                            //entity.RecordStatusUpdateDt = itm.RecordStatusUpdateDt;
                        }
                    }
                    //db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
                return false;
            }
            return true;
        }

        public List<PrintBUModel> GetPrintBUList()
        {
            using (var db = new PCAEntities())
            {
                List<PrintBUModel> PrintBUList = new List<PrintBUModel>();
                var List = (from PrintBUData in db.tblCardMasters
                            join PrintBUMaster in db.tblPreferredBUMasters on PrintBUData.PrintBU equals PrintBUMaster.Code
                            where PrintBUData.PictureUpdated && PrintBUData.SendInPrintFile == (int)Enums.Enum.SendInPrintFile.Print
                            select PrintBUMaster);
                PrintBUList.Add(new PrintBUModel() { ID = "0", Text = "All" });
                foreach (var rec in List.Select(x => new PrintBUModel() { ID = x.Code, Text = x.PreferredBUValue }).Distinct().ToList())
                {
                    PrintBUList.Add(rec);
                }
                return PrintBUList;
            }
        }

        internal List<ExportToXlsModel> GetAckExportList(ExportModelList model)
        {
            try
            {
                var list = new List<ExportToXlsModel>();
                var preferdBuService = new PreferredBUService();
                var printBuList = preferdBuService.GetPrintBUList();

                var coBarndService = new CoBrandedService();
                var coBrandList = coBarndService.GetCoBrandedList();

                string printBuStr = string.Empty;
                string cardTypeStr = string.Empty;

                using (var db = new PCAEntities())
                {
                    var List = db.tblCardMasters.Where(x => x.SendAck);
                    foreach (var item in model.MainExportModel.Where(x => x.IsChecked))
                    {
                        var enty = List.FirstOrDefault(x => x.RecordID == item.RecordID);
                        if (enty.PrintBU != null)
                        {
                            try
                            {
                                var printBU = printBuList.Where(x => x.Code == enty.PrintBU.Trim()).FirstOrDefault();
                                printBuStr = printBU == null ? "" : printBU.PreferredBUValue;
                            }
                            catch (Exception)
                            {
                            }

                        }

                        if (enty.CoBrandedCardType != null)
                        {
                            try
                            {
                                var coBrandedCardType = coBrandList.Where(x => x.Code == enty.CoBrandedCardType).FirstOrDefault();
                                cardTypeStr = coBrandedCardType == null ? "" : coBrandedCardType.CoBrandedValue;
                            }
                            catch (Exception)
                            {

                            }
                        }

                        list.Add(new ExportToXlsModel()
                        {
                            RecordId = enty.RecordID,
                            ActionFlags = enty.ActionFlags,
                            NameOnCard = enty.NameOnCard,
                            PAssionCardNumber = enty.PAssionCardNumber,
                            IDNumber = enty.IDNumber,
                            ExpiryDate = enty.ExpiryDate,
                            HA_BlockHouseNo = enty.HA_BlockHouseNo,
                            HA_StreetName = enty.HA_StreetName,
                            HA_FloorNo = enty.HA_FloorNo,
                            HA_UnitNo = enty.HA_UnitNo,
                            HA_BuildingName = enty.HA_BuildingName,
                            HA_PostalCode = enty.HA_PostalCode,
                            Gender = enty.Gender,

                            PrintBU = enty.PrintBU,
                            PrintBUStr = printBuStr,

                            Code = enty.CoBrandedCardType,
                            CoBrandedCardType = enty.CoBrandedCardType,
                            CoBrandedCardTypeStr = cardTypeStr,
                            WV = enty.WV,
                            PAYM = enty.PAYM,
                            CoBranded = enty.CoBranded,
                            CANNumber = enty.CardCANNumber,
                            OldPassionezLinkNumber = enty.OldPassionezLinkNumber

                        });

                        printBuStr = string.Empty;
                        cardTypeStr = string.Empty;
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
            }
            return null;
        }

        public List<ExportToXlsModel> GetExcelExportList(ExportModelList model)
        {
            try
            {
                var list = new List<ExportToXlsModel>();
                var preferdBuService = new PreferredBUService();
                var printBuList = preferdBuService.GetPrintBUList();

                var coBarndService = new CoBrandedService();
                var coBrandList = coBarndService.GetCoBrandedList();

                string printBuStr = string.Empty;
                string cardTypeStr = string.Empty;

                using (var db = new PCAEntities())
                {
                    var List = db.tblCardMasters.Where(x => x.PictureUpdated && x.RecordStatus == Enums.Enum.RecordStatus.PR.ToString());
                    foreach (var item in model.MainExportModel.Where(x => x.IsChecked))
                    {
                        var enty = List.FirstOrDefault(x => x.RecordID == item.RecordID);
                        if (enty.PrintBU != null)
                        {
                            var printBU = printBuList.Where(x => x.Code == enty.PrintBU.Trim()).FirstOrDefault();
                            printBuStr = printBU == null ? "" : printBU.PreferredBUValue;
                        }

                        if (enty.CoBrandedCardType != null)
                        {
                            var coBrandedCardType = coBrandList.Where(x => x.Code == enty.CoBrandedCardType).FirstOrDefault();
                            cardTypeStr = coBrandedCardType == null ? "" : coBrandedCardType.CoBrandedValue;
                        }

                        list.Add(new ExportToXlsModel()
                        {
                            RecordId = enty.RecordID,
                            ActionFlags = enty.ActionFlags,
                            NameOnCard = enty.NameOnCard,
                            PAssionCardNumber = enty.PAssionCardNumber,
                            IDNumber = enty.IDNumber,
                            ExpiryDate = enty.ExpiryDate,
                            ExpiryDateStr = enty.ExpiryDate.ToString("dd-MMM-yyyy").ToUpper(),
                            HA_BlockHouseNo = enty.HA_BlockHouseNo,
                            HA_StreetName = enty.HA_StreetName,
                            HA_FloorNo = enty.HA_FloorNo,
                            HA_UnitNo = enty.HA_UnitNo,
                            HA_BuildingName = enty.HA_BuildingName,
                            HA_PostalCode = enty.HA_PostalCode,
                            Gender = enty.Gender,

                            PrintBU = enty.PrintBU,
                            PrintBUStr = printBuStr,

                            Code = enty.CoBrandedCardType,
                            CoBrandedCardType = enty.CoBrandedCardType,
                            CoBrandedCardTypeStr = cardTypeStr,
                            WV = enty.WV,
                            PAYM = enty.PAYM,
                            CoBranded = enty.CoBranded,
                        });

                        printBuStr = string.Empty;
                        cardTypeStr = string.Empty;
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
            }
            return null;
        }

        /// <summary>
        /// get iamge by  id number
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        internal byte[] getImageData(string img)
        {
            try
            {
                using (var db = new PCAEntities())
                {
                    var cardtbl = db.tblCardMasters.Where(x => x.IDNumber == img).FirstOrDefault();
                    if (cardtbl != null)
                    {
                        if (cardtbl.PictureUpdated && cardtbl.Picture != null)
                            return cardtbl.Picture;
                    }
                }
                return new byte[1];
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
            }
            return null;
        }

        /// <summary>
        /// updates flagas after export the ack file
        /// </summary>
        /// <param name="list"></param>
        internal int SaveAckFileSendDetails(string opFileName, List<ExportToXlsModel> list)
        {

            using (var db = new PCAEntities())
            {
                using (var trans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var ftRecord = new tblFileMaster();
                        ftRecord.FileName = opFileName;
                        ftRecord.FileType = (int)Enums.Enum.FileTypes.ACKExport;
                        ftRecord.RecordCreatedDate = DateTime.Now;
                        ftRecord.FooterRecordTotals = list.Count;
                        ftRecord.UserID = ConstantVar.CurrentUserId;
                        db.tblFileMasters.Add(ftRecord);

                        int maxId = 0;
                        if (db.tblFileMasters.Count() > 0)
                            maxId = db.tblFileMasters.Max(x => x.RecordID) + 1;
                        else
                            maxId = 1;

                        foreach (var itm in list)
                        {
                            var entity = db.tblCardMasters.Where(x => x.PAssionCardNumber == itm.PAssionCardNumber && x.IDNumber == itm.IDNumber).FirstOrDefault();
                            if (entity != null)
                            {
                                entity.RecordModifiedDt = DateTime.Now;
                                entity.RecordStatus = Enums.Enum.RecordStatus.RD.ToString();
                                //entity.SendInPrintFile = (int)Enums.Enum.SendInPrintFile.Print;
                                //entity.AckFileRefId = ftRecord.RecordID;
                                entity.AckFileRefId = maxId;
                                entity.SendAck = false;
                            }
                        }
                        db.SaveChanges();
                        trans.Commit();
                        return ftRecord.RecordID;
                    }
                    catch (Exception ex)
                    {
                        ExceptionHelper.ExceptionTrack(ex);
                        trans.Rollback();
                        return 0;
                    }
                }
            }

        }

        /// <summary>
        /// update the status of print file and save the print file
        /// </summary>
        /// <param name="opFileName"></param>
        /// <param name="list"></param>
        internal int SavePrintFileDetails(string opFileName, List<ExportToXlsModel> list)
        {

            using (var db = new PCAEntities())
            {
                using (var trans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var ftRecord = new tblFileMaster();
                        ftRecord.FileName = opFileName;
                        ftRecord.FileType = (int)Enums.Enum.FileTypes.ExcelExport;
                        ftRecord.RecordCreatedDate = DateTime.Now;
                        ftRecord.FooterRecordTotals = list.Count;
                        ftRecord.UserID = ConstantVar.CurrentUserId;
                        db.tblFileMasters.Add(ftRecord);
                        //db.SaveChanges();
                        int maxId = 0;
                        if (db.tblFileMasters.Count() > 0)
                            maxId = db.tblFileMasters.Max(x => x.RecordID) + 1;
                        else
                            maxId = 1;
                        //var maxId = db.tblFileMasters.Max(x => x.RecordID) + 1;
                        foreach (var itm in list)
                        {
                            var entity = db.tblCardMasters.Where(x => x.PAssionCardNumber == itm.PAssionCardNumber && x.IDNumber == itm.IDNumber).FirstOrDefault();
                            if (entity != null)
                            {
                                //entity.RecordStatusUpdateDt = DateTime.Now;
                                entity.RecordModifiedDt = DateTime.Now;
                                entity.RecordStatus = Enums.Enum.RecordStatus.SP.ToString();
                                entity.SendInPrintFile = (int)Enums.Enum.SendInPrintFile.Print;
                                //entity.PrintFileRefId = ftRecord.RecordID;
                                entity.PrintFileRefId = maxId;
                            }
                        }
                        db.SaveChanges();
                        trans.Commit();
                        return ftRecord.RecordID;
                    }
                    catch (Exception ex)
                    {
                        ExceptionHelper.ExceptionTrack(ex);
                        trans.Rollback();
                        return 0;
                    }
                }
            }

        }

        /// <summary>
        /// save the tapping can number and expiry date string into database
        /// </summary>
        /// <param name="idnumber"></param>
        /// <param name="pAssionCardNumber"></param>
        /// <param name="cardCANNumber"></param>
        /// <param name="cardCANExpiryDt"></param>
        internal void SaveTappingDetails(string idnumber, string pAssionCardNumber, string cardCANNumber, string cardCANExpiryDt)
        {
            try
            {
                decimal tempDecimal = 0;
                decimal.TryParse(pAssionCardNumber, out tempDecimal);

                using (var db = new PCAEntities())
                {
                    var entity = db.tblCardMasters.Where(x => x.PAssionCardNumber == tempDecimal && x.IDNumber.Equals(idnumber)).FirstOrDefault();
                    if (entity != null)
                    {
                        tempDecimal = 0;
                        decimal.TryParse(cardCANNumber, out tempDecimal);
                        entity.CardCANNumber = tempDecimal;
                        entity.CardCANExpiryDt = cardCANExpiryDt;
                        entity.SendAck = true;
                        entity.SendInPrintFile = (int)Enums.Enum.SendInPrintFile.CANUpdated;
                        entity.RecordStatus = Enums.Enum.RecordStatus.PC.ToString();
                        entity.RecordModifiedDt = DateTime.Now;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
            }
        }

        /// <summary>
        /// get list for the export the records for excel
        /// </summary>
        /// <returns></returns>
        public List<ExportToXlsModel> GetExcelExportList()
        {
            int Srno = 1;
            try
            {
                var list = new List<ExportToXlsModel>();
                var preferdBuService = new PreferredBUService();
                var printBuList = preferdBuService.GetPrintBUList();

                var coBarndService = new CoBrandedService();
                var coBrandList = coBarndService.GetCoBrandedList();

                string printBuStr = string.Empty;
                string cardTypeStr = string.Empty;


                using (var db = new PCAEntities())
                {
                    foreach (var enty in db.tblCardMasters.Where(x => x.PictureUpdated && x.RecordStatus == Enums.Enum.RecordStatus.PR.ToString()).OrderBy(x => x.RecordStatusUpdateDt))
                    {
                        if (enty.PrintBU != null)
                        {
                            var printBU = printBuList.Where(x => x.Code == enty.PrintBU.Trim()).FirstOrDefault();
                            printBuStr = printBU == null ? "" : printBU.PreferredBUValue;
                        }

                        if (enty.CoBrandedCardType != null)
                        {
                            var coBrandedCardType = coBrandList.Where(x => x.Code == enty.CoBrandedCardType).FirstOrDefault();
                            cardTypeStr = coBrandedCardType == null ? "" : coBrandedCardType.CoBrandedValue;
                        }

                        list.Add(new ExportToXlsModel()
                        {

                            RecordId = enty.RecordID,
                            ActionFlags = enty.ActionFlags,
                            NameOnCard = enty.NameOnCard,
                            PAssionCardNumber = enty.PAssionCardNumber,
                            IDNumber = enty.IDNumber,
                            ExpiryDate = enty.ExpiryDate,
                            ExpiryDateStr = enty.ExpiryDate.ToString("dd-MMM-yyyy").ToUpper(),
                            HA_BlockHouseNo = enty.HA_BlockHouseNo,
                            HA_StreetName = enty.HA_StreetName,
                            HA_FloorNo = enty.HA_FloorNo,
                            HA_UnitNo = enty.HA_UnitNo,
                            HA_BuildingName = enty.HA_BuildingName,
                            HA_PostalCode = enty.HA_PostalCode,
                            Gender = enty.Gender,

                            PrintBU = enty.PrintBU,
                            PrintBUStr = printBuStr,

                            Code = enty.CoBrandedCardType,
                            CoBrandedCardType = enty.CoBrandedCardType,
                            CoBrandedCardTypeStr = cardTypeStr,
                            WV = enty.WV,
                            PAYM = enty.PAYM,
                            CoBranded = enty.CoBranded,
                            SrNo = Srno++,
                        });

                        printBuStr = string.Empty;
                        cardTypeStr = string.Empty;
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
            }
            return null;
        }

        /// <summary>
        /// get export list for the ack
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public List<ExportToXlsModel> GetAckExportList()
        {
            int SrNum = 1;
            try
            {
                var list = new List<ExportToXlsModel>();
                var preferdBuService = new PreferredBUService();
                var printBuList = preferdBuService.GetPrintBUList();

                var coBarndService = new CoBrandedService();
                var coBrandList = coBarndService.GetCoBrandedList();

                string printBuStr = string.Empty;
                string cardTypeStr = string.Empty;

                using (var db = new PCAEntities())
                {
                    foreach (var enty in db.tblCardMasters.Where(x => x.SendAck).OrderBy(x => x.RecordStatusUpdateDt))
                    {
                        if (enty.PrintBU != null)
                        {
                            try
                            {
                                var printBU = printBuList.Where(x => x.Code == enty.PrintBU.Trim()).FirstOrDefault();
                                printBuStr = printBU == null ? "" : printBU.PreferredBUValue;
                            }
                            catch (Exception)
                            {
                            }

                        }

                        if (enty.CoBrandedCardType != null)
                        {
                            try
                            {
                                var coBrandedCardType = coBrandList.Where(x => x.Code == enty.CoBrandedCardType).FirstOrDefault();
                                cardTypeStr = coBrandedCardType == null ? "" : coBrandedCardType.CoBrandedValue;
                            }
                            catch (Exception)
                            {

                            }
                        }

                        list.Add(new ExportToXlsModel()
                        {
                            RecordId = enty.RecordID,
                            ActionFlags = enty.ActionFlags,
                            NameOnCard = enty.NameOnCard,
                            PAssionCardNumber = enty.PAssionCardNumber,
                            IDNumber = enty.IDNumber,
                            ExpiryDate = enty.ExpiryDate,
                            HA_BlockHouseNo = enty.HA_BlockHouseNo,
                            HA_StreetName = enty.HA_StreetName,
                            HA_FloorNo = enty.HA_FloorNo,
                            HA_UnitNo = enty.HA_UnitNo,
                            HA_BuildingName = enty.HA_BuildingName,
                            HA_PostalCode = enty.HA_PostalCode,
                            Gender = enty.Gender,

                            PrintBU = enty.PrintBU,
                            PrintBUStr = printBuStr,

                            Code = enty.CoBrandedCardType,
                            CoBrandedCardType = enty.CoBrandedCardType,
                            CoBrandedCardTypeStr = cardTypeStr,
                            WV = enty.WV,
                            PAYM = enty.PAYM,
                            CoBranded = enty.CoBranded,
                            CANNumber = enty.CardCANNumber,
                            OldPassionezLinkNumber = enty.OldPassionezLinkNumber,
                            SrNo = SrNum++

                        });

                        printBuStr = string.Empty;
                        cardTypeStr = string.Empty;
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
            }
            return null;
        }

        /// <summary>
        /// Get card tapping list
        /// </summary>
        /// <returns></returns>
        public IList<CardTappingModel> GetTappingList()
        {
            try
            {
                var list = new List<CardTappingModel>();
                using (var db = new PCAEntities())
                {
                    var printBUList = db.tblPreferredBUMasters.ToList();
                    foreach (var enty in db.tblCardMasters.Where(x => x.PictureUpdated && x.SendInPrintFile == (int)Enums.Enum.SendInPrintFile.Print).OrderBy(x => x.RecordStatusUpdateDt))
                    {
                        var itm = new CardTappingModel();
                        itm.Gender = enty.Gender;
                        itm.PAssionCardNumber = enty.PAssionCardNumber;
                        itm.RecordID = enty.RecordID;
                        itm.IDNumber = enty.IDNumber;
                        itm.NameOnCard = enty.NameOnCard;
                        itm.PAssionCardNumberStr = enty.PAssionCardNumber.ToString().ToPMembership();
                        if (enty.PrintBU != null)
                        {
                            var selectedBU = printBUList.FirstOrDefault(x => x.Code == enty.PrintBU.Trim());
                            if (selectedBU != null)
                            {
                                itm.PrintBU = selectedBU.PreferredBUValue;
                            }
                        }

                        itm.CardCANExpiryDt = enty.ExpiryDate.ToString("dd MMM yyyy");

                        list.Add(itm);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
            }
            return null;
        }
    }
}