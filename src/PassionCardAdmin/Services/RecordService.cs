﻿using Microsoft.Reporting.WebForms;
using PassionCardAdmin.Helpers;
using PassionCardAdmin.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PassionCardAdmin.Services
{
    public class RecordService
    {
        #region "Record Region"

        /// <summary>
        /// Returns record List as per filter applied on view.
        /// </summary>
        /// <param name="formParams"></param>
        /// <returns></returns>
        internal object GetRecordList(System.Collections.Specialized.NameValueCollection formParams)
        {
            var form = new GenericListParams(formParams);

            int count = 0;
            decimal totalPages = 0;
            int page = 0;

            using (var db = new PCAEntities())
            {
                var query = db.tblCardMasters.AsQueryable();

                List<CardMasterModel> list = new List<CardMasterModel>();

                var sortColumn = ParamsHelper.GetStringParam(form.Params["SortColumn"]);
                var sortOrder = ParamsHelper.GetStringParam(form.Params["SortOrder"]);
                var paCardNumber = ParamsHelper.GetLongParam(form.Params["PACardNumber"]);
                var IDNumber = ParamsHelper.GetStringParam(form.Params["IDNumber"]);
                var Birthday = ParamsHelper.GetNullableDate(form.Params["Birthday"]);
                var CanNO = ParamsHelper.GetLongParam(form.Params["CanNO"]);
                var Status = ParamsHelper.GetStringParam(form.Params["Status"]);
                var NameOnCard = ParamsHelper.GetStringParam(form.Params["NameOnCard"]);
                var CanNumber = ParamsHelper.GetLongParam(form.Params["CanNumber"]);
                var FromDate = ParamsHelper.GetNullableDate(form.Params["FromDate"]);
                var ToDate = ParamsHelper.GetNullableDate(form.Params["ToDate"]);

                query = GetFilteredQuery(query, sortColumn, sortOrder, paCardNumber, IDNumber, Birthday, Status, NameOnCard, CanNumber, FromDate, ToDate);

                //calculate total no of pages
                count = query.Count();
                totalPages = ParamsHelper.TotalPages(count, form.Limit);
                page = ParamsHelper.Page(form.Page, totalPages);

                var returnList = query
                    .Skip(form.Start)
                    .Take(form.Limit)
                    .ToList();
                var preferedBUList = db.tblPreferredBUMasters.ToList();
                var coBrandedTypeList = db.tblCoBrandedMasters.ToList();
                int srNo = form.Start;

                foreach (var model in returnList)
                {
                    var temp = new CardMasterModel();
                    var pbu = preferedBUList.FirstOrDefault(x => x.Code == model.PreferredBU);
                    var cbm = coBrandedTypeList.FirstOrDefault(x => x.Code == model.CoBrandedCardType);
                    list.Add(temp.CopyFromDbToModel(model, (pbu != null) ? pbu.PreferredBUValue : "", (cbm != null) ? cbm.CoBrandedValue : "", ++srNo));
                }

                return new CardListPagingModel
                {
                    page = page,
                    total = totalPages,
                    records = count,
                    data = list
                };
            }
        }

        /// <summary>
        /// Export PrintBU data to Excel
        /// </summary>
        /// <param name="data"></param>
        public void ExportPrintBUReport(List<CardMasterModel> data, PrintBUReportModel dateModel, bool IsPDF, HttpResponseBase response, HttpServerUtilityBase server)
        {

            string FromToDateString = dateModel.FromDate.ToString("dd MMM yyyy") + " To " + ((dateModel.ToDate == DateTime.MinValue) ? DateTime.MaxValue.ToString("dd MMM yyyy") : dateModel.ToDate.ToString("dd MMM yyyy"));
            string TodaysDate = DateTime.Now.ToString("dd MMM yyyy");
            string PrintedBy = Utilities.ConstantVar.CurrentUserName;

            LocalReport localReport = new LocalReport();

            localReport.ReportPath = server.MapPath(Utilities.ConstantVar.ReportPath + "PrintBU.rdlc");

            //Passing Parameters                
            localReport.SetParameters(new ReportParameter("FromToDateString", FromToDateString));
            localReport.SetParameters(new ReportParameter("TodaysDate", TodaysDate));
            localReport.SetParameters(new ReportParameter("PrintedBy", PrintedBy));

            ReportDataSource reportDataSource = new ReportDataSource("PrintBUReportDataset", GetPrintBUModel(data));
            localReport.DataSources.Clear();
            localReport.DataSources.Add(reportDataSource);

            localReport.Refresh();

            string extensionString = "xls";
            string outputType = "Excel";

            if (IsPDF)
            {
                extensionString = "Pdf";
                outputType = "Pdf";
            }

            ExportReportHelper.ReportExportHelper(localReport, outputType, "PrintBUReport", true, extensionString, response);
        }

        private object GetPrintBUModel(List<CardMasterModel> data)
        {
            List<PrintBUReportDataModel> returnList = new List<PrintBUReportDataModel>();
            foreach (var item in data)
            {
                PrintBUReportDataModel m = new PrintBUReportDataModel();
                returnList.Add(m.CopyToReportModel(item));
            }
            return returnList;
            //return data.Select(x => new PrintBUReportDataModel().CopyToReportModel(x));
        }

        /// <summary>
        /// Filter query bydetails as per need.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <param name="paCardNumber"></param>
        /// <param name="iDNumber"></param>
        /// <param name="birthday"></param>
        /// <param name="status"></param>
        /// <param name="nameOnCard"></param>
        /// <param name="canNumber"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        private IQueryable<tblCardMaster> GetFilteredQuery(IQueryable<tblCardMaster> query, string sortColumn, string sortOrder, long paCardNumber, string iDNumber, DateTime? birthday, string status, string nameOnCard, long canNumber, DateTime? fromDate, DateTime? toDate)
        {
            //Filter by fromDate
            if (fromDate.HasValue && fromDate.Value != DateTime.MinValue)
            {
                var date = fromDate.Value.Date;
                query = query.Where(x => x.RecordCreatedDt >= date);
            }

            //Filter by toDate
            if (toDate.HasValue && toDate.Value != DateTime.MinValue)
            {
                var date = toDate.Value.Date.AddDays(1);
                query = query.Where(x => x.RecordCreatedDt <= date);
            }

            //Filter by paCardNumber
            if (paCardNumber != 0)
            {
                query = query.Where(x => x.PAssionCardNumber == paCardNumber);
            }

            //Filter by iDNumber
            if (!string.IsNullOrEmpty(iDNumber) || !string.IsNullOrWhiteSpace(iDNumber))
            {
                query = query.Where(x => x.IDNumber.Equals(iDNumber, StringComparison.OrdinalIgnoreCase));
            }

            //Filter by birthday
            if (birthday != null)
            {
                query = query.Where(x => x.Birthday == birthday);
            }

            //Filter by canNumber
            if (canNumber != 0)
            {
                query = query.Where(x => x.CardCANNumber == canNumber);
            }

            //Filter by status
            if (!string.IsNullOrEmpty(status) || !string.IsNullOrWhiteSpace(status))
            {
                query = query.Where(x => x.RecordStatus.Equals(status, StringComparison.OrdinalIgnoreCase));
            }

            //Filter by Name on card
            if (!string.IsNullOrEmpty(nameOnCard) || !string.IsNullOrWhiteSpace(nameOnCard))
            {
                query = query.Where(x => x.NameOnCard.Equals(nameOnCard, StringComparison.OrdinalIgnoreCase));
            }

            //Sort by column by Option.
            if (!string.IsNullOrEmpty(sortOrder) && !string.IsNullOrEmpty(sortColumn))
            {
                query = GetSortedColumn(query, sortColumn.ToLower(), sortOrder.ToLower());
            }

            return query;
        }

        private IQueryable<tblCardMaster> GetSortedColumn(IQueryable<tblCardMaster> query, string sortColumn, string sortOrder)
        {
            switch (sortColumn.ToLower())
            {
                case "record id":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.RecordID);
                    else
                        return query.OrderByDescending(x => x.RecordID);

                case "action flags":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.ActionFlags);
                    else
                        return query.OrderByDescending(x => x.ActionFlags);

                case "name":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.Name);
                    else
                        return query.OrderByDescending(x => x.Name);

                case "name on card":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.NameOnCard);
                    else
                        return query.OrderByDescending(x => x.NameOnCard);

                case "id type":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.IDType);
                    else
                        return query.OrderByDescending(x => x.IDType);

                case "id number":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.IDNumber);
                    else
                        return query.OrderByDescending(x => x.IDNumber);

                case "passion card number":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.PAssionCardNumber);
                    else
                        return query.OrderByDescending(x => x.PAssionCardNumber);

                case "preferred bu":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.PreferredBU);
                    else
                        return query.OrderByDescending(x => x.PreferredBU);

                case "email address":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.EmailAddress);
                    else
                        return query.OrderByDescending(x => x.EmailAddress);

                case "ma block house no":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.MA_BlockHouseNo);
                    else
                        return query.OrderByDescending(x => x.MA_BlockHouseNo);

                case "ma street name":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.MA_StreetName);
                    else
                        return query.OrderByDescending(x => x.MA_StreetName);

                case "ma floor no":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.MA_FloorNo);
                    else
                        return query.OrderByDescending(x => x.MA_FloorNo);

                case "ma unit no":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.MA_UnitNo);
                    else
                        return query.OrderByDescending(x => x.MA_UnitNo);

                case "ma building name":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.MA_BuildingName);
                    else
                        return query.OrderByDescending(x => x.MA_BuildingName);

                case "ma postal code":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.MA_PostalCode);
                    else
                        return query.OrderByDescending(x => x.MA_PostalCode);

                case "ha blockhouse no":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.HA_BlockHouseNo);
                    else
                        return query.OrderByDescending(x => x.HA_BlockHouseNo);

                case "ha street name":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.HA_StreetName);
                    else
                        return query.OrderByDescending(x => x.HA_StreetName);

                case "ha floor no":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.HA_FloorNo);
                    else
                        return query.OrderByDescending(x => x.HA_FloorNo);

                case "ha unit no":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.HA_UnitNo);
                    else
                        return query.OrderByDescending(x => x.HA_UnitNo);

                case "ha building name":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.HA_BuildingName);
                    else
                        return query.OrderByDescending(x => x.HA_BuildingName);

                case "ha postal code":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.HA_PostalCode);
                    else
                        return query.OrderByDescending(x => x.HA_PostalCode);

                case "expiry date":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.ExpiryDate);
                    else
                        return query.OrderByDescending(x => x.ExpiryDate);

                case "hp number":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.HpNumber);
                    else
                        return query.OrderByDescending(x => x.HpNumber);

                case "contact number home":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.ContactNumberHome);
                    else
                        return query.OrderByDescending(x => x.ContactNumberHome);

                case "wv":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.WV);
                    else
                        return query.OrderByDescending(x => x.WV);

                case "paym":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.PAYM);
                    else
                        return query.OrderByDescending(x => x.PAYM);

                case "cobranded":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.CoBranded);
                    else
                        return query.OrderByDescending(x => x.CoBranded);

                case "cobranded card type":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.CoBrandedCardType);
                    else
                        return query.OrderByDescending(x => x.CoBrandedCardType);

                case "card type flag":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.CardTypeFlag);
                    else
                        return query.OrderByDescending(x => x.CardTypeFlag);

                case "birthday":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.Birthday);
                    else
                        return query.OrderByDescending(x => x.Birthday);

                case "gender":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.Gender);
                    else
                        return query.OrderByDescending(x => x.Gender);

                case "marital status":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.MaritalStatus);
                    else
                        return query.OrderByDescending(x => x.MaritalStatus);

                case "annual income":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.AnnualIncome);
                    else
                        return query.OrderByDescending(x => x.AnnualIncome);

                case "housing type":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.HousingType);
                    else
                        return query.OrderByDescending(x => x.HousingType);

                case "highest education level":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.HighestEducationLevel);
                    else
                        return query.OrderByDescending(x => x.HighestEducationLevel);

                case "print bu":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.PrintBU);
                    else
                        return query.OrderByDescending(x => x.PrintBU);

                case "preferred wv":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.PreferredWV);
                    else
                        return query.OrderByDescending(x => x.PreferredWV);

                case "billing bu":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.BillingBU);
                    else
                        return query.OrderByDescending(x => x.BillingBU);

                case "old passionez link number":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.OldPassionezLinkNumber);
                    else
                        return query.OrderByDescending(x => x.OldPassionezLinkNumber);

                case "nationality":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.Nationality);
                    else
                        return query.OrderByDescending(x => x.Nationality);

                case "title":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.Title);
                    else
                        return query.OrderByDescending(x => x.Title);

                case "race":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.Race);
                    else
                        return query.OrderByDescending(x => x.Race);

                case "occupation":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.Occupation);
                    else
                        return query.OrderByDescending(x => x.Occupation);

                case "user id":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.UserId);
                    else
                        return query.OrderByDescending(x => x.UserId);

                case "file master id":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.FileMasterId);
                    else
                        return query.OrderByDescending(x => x.FileMasterId);

                case "record created date":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.RecordCreatedDt);
                    else
                        return query.OrderByDescending(x => x.RecordCreatedDt);

                case "record modified date":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.RecordModifiedDt);
                    else
                        return query.OrderByDescending(x => x.RecordModifiedDt);

                case "picture updated":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.PictureUpdated);
                    else
                        return query.OrderByDescending(x => x.PictureUpdated);

                case "picture file":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.PictureFile);
                    else
                        return query.OrderByDescending(x => x.PictureFile);

                case "picture":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.Picture);
                    else
                        return query.OrderByDescending(x => x.Picture);

                case "send in print file":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.SendInPrintFile);
                    else
                        return query.OrderByDescending(x => x.SendInPrintFile);

                case "print file ref id":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.PrintFileRefId);
                    else
                        return query.OrderByDescending(x => x.PrintFileRefId);

                case "card can number":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.CardCANNumber);
                    else
                        return query.OrderByDescending(x => x.CardCANNumber);

                case "card can expiry date":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.CardCANExpiryDt);
                    else
                        return query.OrderByDescending(x => x.CardCANExpiryDt);

                case "send ack":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.SendAck);
                    else
                        return query.OrderByDescending(x => x.SendAck);

                case "ack file ref id":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.AckFileRefId);
                    else
                        return query.OrderByDescending(x => x.AckFileRefId);

                case "record status":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.RecordStatus);
                    else
                        return query.OrderByDescending(x => x.RecordStatus);

                case "record status update date":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.RecordStatusUpdateDt);
                    else
                        return query.OrderByDescending(x => x.RecordStatusUpdateDt);

                default:
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.RecordID);
                    else
                        return query.OrderByDescending(x => x.RecordID);
            }
        }

        #endregion

        #region Summary Region

        /// <summary>
        /// Get summary details by type
        /// </summary>
        /// <param name="formParams"></param>
        /// <returns></returns>
        internal List<SummaryDetail> GetDataByType(System.Collections.Specialized.NameValueCollection formParams)
        {
            var form = new GenericListParams(formParams);

            var txtSelectedDate = ParamsHelper.GetNullableDate(form.Params["SelectedDate"]);
            var txtSelectedType = ParamsHelper.GetStringParam(form.Params["SelectedType"]);//Weekly  Monthly
            if (txtSelectedDate.HasValue)
            {
                return GetFilteredDate(txtSelectedDate, txtSelectedType);
            }

            return null;
        }

        /// <summary>
        /// Get Summary details from database
        /// </summary>
        /// <param name="txtSelectedDate"></param>
        /// <param name="txtSelectedType"></param>
        /// <returns></returns>
        private List<SummaryDetail> GetFilteredDate(DateTime? txtSelectedDate, string txtSelectedType)
        {
            List<SummaryDetail> returnList = new List<SummaryDetail>();

            using (var db = new PCAEntities())
            {
                DateTime stratDate, endDate;

                if (txtSelectedType.ToLower().Equals("monthly"))
                {
                    stratDate = new DateTime(txtSelectedDate.Value.Year, txtSelectedDate.Value.Month, 1);
                    endDate = stratDate.AddMonths(1).AddSeconds(-1);
                }
                else
                {
                    stratDate = txtSelectedDate.Value.FirstDayOfWeek();
                    endDate = txtSelectedDate.Value.LastDayOfWeek().AddDays(1).AddSeconds(-1);
                }
                var dtRange = GetDateRange(stratDate, endDate);

                var CardList = db.tblCardMasters.AsQueryable();
                //CardList = CardList.Where(x => x.PictureUpdated == true && x.RecordStatus == Enums.Enum.RecordStatus.PR.ToString());
                CardList = CardList.Where(x => x.PictureUpdated == true);

                var CardDetails = from trans in CardList
                                  where trans.RecordStatusUpdateDt >= stratDate && trans.RecordStatusUpdateDt <= endDate
                                  group trans by DbFunctions.TruncateTime(trans.RecordStatusUpdateDt) into empg
                                  select new
                                  {
                                      DateStr = empg.Key,
                                      PictureCount = empg.Count()
                                  };

                var dateWiseList = CardDetails.ToList();

                foreach (var dt in dtRange)
                {
                    SummaryDetail obj = new SummaryDetail();
                    if (txtSelectedType.ToLower().Equals("monthly"))
                        obj.Title = dt.ToString("dd MMM yyyy");
                    else
                        obj.Title = dt.ToString("dddd");

                    var o = dateWiseList.Where(x => x.DateStr.Value == dt).FirstOrDefault();
                    if (o != null)
                        obj.PictureCountValue = (double)(o.PictureCount);

                    returnList.Add(obj);
                }
            }
            return returnList;
        }

        /// <summary>
        /// Returns date range
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public IEnumerable<DateTime> GetDateRange(DateTime startDate, DateTime endDate)
        {
            while (startDate <= endDate)
            {
                yield return startDate;
                startDate = startDate.AddDays(1);
            }
        }


        #endregion

        #region "Print BU Report"

        internal List<CardMasterModel> GetPrintBUReport(PrintBUReportModel model)
        {
            var dataList = new List<CardMasterModel>();

            using (var db = new PCAEntities())
            {
                var query = db.tblCardMasters.AsQueryable();

                //Filter by fromDate
                if (model.FromDate != DateTime.MinValue)
                {
                    var date = model.FromDate;
                    query = query.Where(x => x.RecordStatusUpdateDt >= date);
                }

                //Filter by toDate
                if (model.ToDate != DateTime.MinValue)
                {
                    var date = model.ToDate.AddDays(1);
                    query = query.Where(x => x.RecordStatusUpdateDt <= date);
                }

                //var returnList = query
                //    .OrderBy(x => x.RecordStatusUpdateDt);

                var returnList = GetTreeGridData(query, db.tblPreferredBUMasters.ToList());

                var preferedBUList = db.tblPreferredBUMasters.ToList();
                var coBrandedTypeList = db.tblCoBrandedMasters.ToList();

                foreach (var item1 in returnList)
                {
                    foreach (var item2 in item1.cardModelList)
                    {
                        foreach (var item in item2.cardModelList)
                        {
                            var temp = new CardMasterModel();
                            var pbu = preferedBUList.FirstOrDefault(x => x.Code == item.PrintBU.Trim());
                            var cbm = coBrandedTypeList.FirstOrDefault(x => x.Code == item.CoBrandedCardType);
                            if (pbu != null)
                                item.PreferedBUStr = pbu.PreferredBUValue;
                            dataList.Add(item);
                        }
                    }
                }
            }

            return dataList;
        }

        /// <summary>
        /// Get summary details by type
        /// </summary>
        /// <param name="formParams"></param>
        /// <returns></returns>
        internal object GetPrintBUReport(System.Collections.Specialized.NameValueCollection formParams)
        {
            var form = new GenericListParams(formParams);

            int count = 0;
            decimal totalPages = 0;
            int page = 0;

            using (var db = new PCAEntities())
            {
                var query = db.tblCardMasters.AsQueryable();

                List<CardMasterModel> list = new List<CardMasterModel>();
                var FromDate = ParamsHelper.GetNullableDate(form.Params["FromDate"]);
                var ToDate = ParamsHelper.GetNullableDate(form.Params["ToDate"]);

                //Filter by fromDate
                if (FromDate.HasValue && FromDate.Value != DateTime.MinValue)
                {
                    var date = FromDate.Value.Date;
                    query = query.Where(x => x.RecordStatusUpdateDt >= date);
                }

                //Filter by toDate
                if (ToDate.HasValue && ToDate.Value != DateTime.MinValue)
                {
                    var date = ToDate.Value.Date.AddDays(1);
                    query = query.Where(x => x.RecordStatusUpdateDt <= date);
                }

                ////calculate total no of pages
                count = query.Count();
                totalPages = ParamsHelper.TotalPages(count, form.Limit);
                page = ParamsHelper.Page(form.Page, totalPages);

                query = query
                    .OrderBy(x => x.RecordStatusUpdateDt)
                    .Skip(form.Start)
                    .Take(form.Limit);

                var data = GetTreeGridData(query, db.tblPreferredBUMasters.ToList());

                return new PrintBUReportModelPagingModel
                {
                    page = page,
                    total = totalPages,
                    records = count,
                    data = data
                };
            }
        }

        /// <summary>
        /// Filter data as per tree grid format.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="BUList"></param>
        /// <returns></returns>
        private List<PrintBUParent1Model> GetTreeGridData(IQueryable<tblCardMaster> list, List<tblPreferredBUMaster> BUList)
        {
            var process1Data = from trans in list
                               group trans by DbFunctions.TruncateTime(trans.RecordStatusUpdateDt) into empg
                               select new
                               {
                                   GroupKey = empg.Key,
                                   GroupValuesList = empg.ToList()
                               };

            List<PrintBUParent1Model> modelList = new List<PrintBUParent1Model>();

            foreach (var p1Data in process1Data)
            {
                var process2Data = from trans in p1Data.GroupValuesList
                                   group trans by trans.PrintBU into empg
                                   select new
                                   {
                                       GroupKey = empg.Key,
                                       GroupValuesList = empg.ToList()
                                   };

                PrintBUParent1Model m2 = new PrintBUParent1Model();
                m2.Title = p1Data.GroupKey.Value.ToString("dd/MM/yyyy");
                foreach (var p2Data in process2Data)
                {
                    PrintBUParent2Model m1 = new PrintBUParent2Model();
                    m1.Title = p2Data.GroupKey;
                    var buData = BUList.FirstOrDefault(x => x.Code == m1.Title.Trim());
                    if (buData != null)
                        m1.PrintBUValue = buData.PreferredBUValue;
                    foreach (var item in p2Data.GroupValuesList)
                    {
                        CardMasterModel Cm = new CardMasterModel();
                        m1.cardModelList.Add(Cm.CopyFromDbToModel(item));
                    }
                    m2.cardModelList.Add(m1);
                }
                modelList.Add(m2);
            }

            return modelList;
        }

        #endregion
    }
}