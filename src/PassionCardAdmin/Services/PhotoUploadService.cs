﻿using PassionCardAdmin.Helpers;
using PassionCardAdmin.Models;
using PassionCardAdmin.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PassionCardAdmin.Services
{
    public class PhotoUploadService
    {
        /// <summary>
        /// Return Data as per ICNumber.
        /// </summary>
        /// <param name="formParams"></param>
        /// <returns></returns>
        internal CardMasterModel GetData(string txtICNumber)
        {
            using (var db = new PCAEntities())
            {
                var query = db.tblCardMasters.AsQueryable();

                var ICNumberData = query.FirstOrDefault(x => x.IDNumber == txtICNumber);

                if (ICNumberData != null)
                {
                    var model= new CardMasterModel().CopyFromDbToModel(ICNumberData);
                    model.RecordStatus = GetStatus(ICNumberData.RecordStatus);
                    return model;
                }
                else
                    return null;
            }
        }

        private string GetStatus(string recordStatus)
        {
            string status = "Photo Not uploaded";

            switch (recordStatus)
            {
                case "PR":
                    status = "Picture Received";
                    break;

                case "SP":
                    status = "Sent to Print";
                    break;

                case "PC":
                    status = "Printing Completed ";
                    break;

                case "RD":
                    status = "Card Ready for Dispatch";
                    break;
                default:
                    break;
            }
            return status;
        }

        /// <summary>
        /// Reset Photo Status to Upload Image
        /// </summary>
        /// <param name="iCNumber"></param>
        /// <returns></returns>
        public bool ResetPhotoStatus(string iCNumber)
        {
            using (var db = new PCAEntities())
            {
                using (var dataContrextTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var query = db.tblCardMasters.AsQueryable();

                        var ICNumberData = query.FirstOrDefault(x => x.IDNumber == iCNumber);

                        if (ICNumberData != null)
                        {
                            ICNumberData.PictureUpdated = false;
                            ICNumberData.PictureFile = null;
                            ICNumberData.Picture = null;
                            ICNumberData.SendInPrintFile = 0;
                            ICNumberData.PrintFileRefId = 0;
                            ICNumberData.CardCANNumber = 0;
                            ICNumberData.CardCANExpiryDt = "";
                            ICNumberData.SendAck = false;
                            ICNumberData.AckFileRefId = 0;
                            ICNumberData.RecordStatus = "N";
                            ICNumberData.RecordStatusUpdateDt = null;


                            tblPhotoResetHistory tblPRH = new tblPhotoResetHistory()
                            {
                                IDNumber = iCNumber,
                                Name = ICNumberData.Name,
                                CreatedDate = DateTime.Now,
                                CreatedById = ConstantVar.CurrentUserId,
                                CreatedByName = ConstantVar.CurrentUserName
                            };

                            db.tblPhotoResetHistories.Add(tblPRH);
                            dataContrextTrans.Commit();
                            db.SaveChanges();
                            return true;
                        }
                        else
                            return false;
                    }
                    catch (Exception ex)
                    {
                        dataContrextTrans.Rollback();
                        ExceptionHelper.ExceptionTrack(ex);
                    }
                }
                return false;
            }
        }
    }
}