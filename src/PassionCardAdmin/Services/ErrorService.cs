﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionCardAdmin.Services
{
    public class ErrorService
    {
        public Dictionary<string, string> Errors { get; set; }

        public List<string> ErrorsString { get; set; }

        public ErrorService()
        {
            Errors = new Dictionary<string, string>();
            ErrorsString = new List<string>();
        }
    }
}