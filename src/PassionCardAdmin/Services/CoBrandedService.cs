﻿using PassionCardAdmin.Helpers;
using PassionCardAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionCardAdmin.Services
{
    public class CoBrandedService
    {
        /// <summary>
        /// get single recod
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public string GetCoBrandedItem(string code)
        {
            try
            {
                string returnValue = string.Empty;
                using (var db = new PCAEntities())
                {
                    var obj = db.tblCoBrandedMasters.Where(x => x.Code == code).FirstOrDefault();
                    if (obj != null)
                        returnValue = obj.CoBrandedValue;
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
            }
            return null;
        }

        /// <summary>
        /// get complete list
        /// </summary>
        /// <returns></returns>
        public List<CoBrandedMasterModel> GetCoBrandedList()
        {
            try
            {
                var list = new List<CoBrandedMasterModel>();
                using (var db = new PCAEntities())
                {
                    foreach (var obj in db.tblCoBrandedMasters)
                    {
                        list.Add(new CoBrandedMasterModel()
                        {
                            Code = obj.Code,
                            CoBrandedValue = obj.CoBrandedValue != null ? obj.CoBrandedValue.Trim() : "",
                            RecordID = obj.RecordID
                        });
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
            }
            return null;
        }
    }
}