﻿using PassionCardAdmin.Helpers;
using PassionCardAdmin.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PassionCardAdmin.Services
{
    public class FileService
    {
        #region "File region"
        /// <summary>
        /// Returns File list as per filter
        /// </summary>
        /// <param name="formParams"></param>
        /// <returns></returns>
        internal object GetFileList(System.Collections.Specialized.NameValueCollection formParams)
        {
            var form = new GenericListParams(formParams);

            int count = 0;
            decimal totalPages = 0;
            int page = 0;

            using (var db = new PCAEntities())
            {
                var query = db.tblFileMasters.AsQueryable();

                List<FileMasterModel> list = new List<FileMasterModel>();

                var txtFileName = ParamsHelper.GetStringParam(form.Params["FileName"]);

                var sortColumn = ParamsHelper.GetStringParam(form.Params["SortColumn"]);
                var sortOrder = ParamsHelper.GetStringParam(form.Params["SortOrder"]);

                query = GetFilteredQuery(query, sortColumn, sortOrder, txtFileName);

                //calculate total no of pages
                count = query.Count();
                totalPages = ParamsHelper.TotalPages(count, form.Limit);
                page = ParamsHelper.Page(form.Page, totalPages);

                var returnList = query
                    .Skip(form.Start)
                    .Take(form.Limit)
                    .ToList();

                foreach (var model in returnList)
                {
                    var temp = new FileMasterModel();
                    list.Add(temp.CopyFromDbModel(model));
                }

                return new FileListPagingModel
                {
                    page = page,
                    total = totalPages,
                    records = count,
                    data = list
                };
            }
        }

        internal CardMasterModel GetRecord(int recordId)
        {
            using (var db = new PCAEntities())
            {
                var CardModel = new CardMasterModel();
                var record = db.tblCardMasters.FirstOrDefault(x => x.RecordID == recordId);
                if (record != null)
                    return CardModel.CopyFromDbToModel(record);
                else
                    return null;
            }
        }

        /// <summary>
        /// filter query depending on data passed in function.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <param name="txtFileName"></param>
        /// <returns></returns>
        public IQueryable<tblFileMaster> GetFilteredQuery(IQueryable<tblFileMaster> query, string sortColumn, string sortOrder, string txtFileName)
        {
            //Filter by File Name.
            if (!string.IsNullOrEmpty(txtFileName))
            {
                query = query.Where(x => x.FileName.Contains(txtFileName));
            }

            //Sort by column by Option.
            if (!string.IsNullOrEmpty(sortOrder) && !string.IsNullOrEmpty(sortColumn))
            {
                query = GetSortedColumn(query, sortColumn.ToLower(), sortOrder.ToLower());
            }
            return query;
        }

        internal bool UpdateCardDetails(CardEditModel model)
        {
            using (var db = new PCAEntities())
            {
                var record = db.tblCardMasters.FirstOrDefault(x => x.RecordID == model.RecordId);
                if (record != null)
                {
                    record.CardCANExpiryDt = model.CardCANExpiryDt;
                    record.CardCANNumber = model.CardCANNumber;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Sort columns as per given options.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        private IQueryable<tblFileMaster> GetSortedColumn(IQueryable<tblFileMaster> query, string sortColumn, string sortOrder)
        {
            switch (sortColumn.ToLower())
            {
                case "record id":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.RecordID);
                    else
                        return query.OrderByDescending(x => x.RecordID);

                case "file name":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.FileName);
                    else
                        return query.OrderByDescending(x => x.FileName);

                case "records":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.FooterRecordTotals);
                    else
                        return query.OrderByDescending(x => x.FooterRecordTotals);

                case "header file id":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.HeaderFileID);
                    else
                        return query.OrderByDescending(x => x.HeaderFileID);

                case "footer file id":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.FooterFileID);
                    else
                        return query.OrderByDescending(x => x.FooterFileID);

                case "record creation date":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.RecordCreatedDate);
                    else
                        return query.OrderByDescending(x => x.RecordCreatedDate);

                case "file type":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.FileType);
                    else
                        return query.OrderByDescending(x => x.FileType);

                default:
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.FileName);
                    else
                        return query.OrderByDescending(x => x.FileName);
            }
        }

        #endregion
    }
}