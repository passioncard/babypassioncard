﻿using PassionCardAdmin.Helpers;
using PassionCardAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionCardAdmin.Services
{
    public class PreferredBUService
    {
        /// <summary>
        /// get single recod
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public string GetPreferredBUItem(string code)
        {
            string returnValue = string.Empty;
            using (var db = new PCAEntities())
            {
                var obj = db.tblPreferredBUMasters.Where(x => x.Code == code).FirstOrDefault();
                if (obj != null)
                    returnValue = obj.PreferredBUValue;
            }
            return returnValue;
        }

        /// <summary>
        /// get complete list
        /// </summary>
        /// <returns></returns>
        public List<PreferredBUMasterModel> GetPrintBUList()
        {
            try
            {
                var list = new List<PreferredBUMasterModel>();
                using (var db = new PCAEntities())
                {
                    foreach (var obj in db.tblPreferredBUMasters)
                    {
                        list.Add(new PreferredBUMasterModel()
                        {
                            Code = obj.Code,
                            PreferredBUValue = obj.PreferredBUValue != null ? obj.PreferredBUValue.Trim() : "",
                            RecordID = obj.RecordID
                        });
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
            }
            return null;
        }
    }
}