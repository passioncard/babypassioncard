﻿using PassionCardAdmin.Helpers;
using PassionCardAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionCardAdmin.Services
{
    public class FileMasterService
    {

        /// <summary>
        /// check is record exist in database or not 
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool IsRecordExist(string fileName)
        {
            using (var db = new PCAEntities())
            {
                var obj = db.tblFileMasters.Where(x => x.FileName == fileName).FirstOrDefault();
                if (obj != null)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// return the record if exist
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public tblFileMaster GetFileMasterRecord(string fileName)
        {
            using (var db = new PCAEntities())
                return db.tblFileMasters.Where(x => x.FileName == fileName).FirstOrDefault();
        }

        /// <summary>
        /// Save file details into database
        /// </summary>
        /// <returns></returns>
        public tblFileMaster SaveOrUpdateFileDetail(FileMasterModel modle, PCAEntities db)
        {
            try
            {
                //using (var db = new PCAEntities())
                {
                    var tblFileMaster = new tblFileMaster();

                    tblFileMaster.HeaderFileID = modle.HeaderFileID;
                    tblFileMaster.HeaderFileCreationDate = modle.HeaderFileCreationDate;
                    tblFileMaster.FooterFileID = modle.FooterFileID;
                    tblFileMaster.FooterRecordTotals = modle.FooterRecordTotals;
                    tblFileMaster.FileName = modle.FileName;
                    tblFileMaster.RecordCreatedDate = modle.RecordCreatedDate;
                    tblFileMaster.FileType = modle.FileType;
                    tblFileMaster.UserID = modle.UserID;

                    db.tblFileMasters.Add(tblFileMaster);
                    //db.SaveChanges();
                    return tblFileMaster;
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
            }
            return null;
        }
    }
}