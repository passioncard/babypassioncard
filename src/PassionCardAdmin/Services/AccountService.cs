﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace PassionCardAdmin.Services
{
    using Helpers;
    using PassionCardAdmin.Models;

    public class AccountService : ErrorService
    {
        public AccountService()
        {

        }


        public UserMasterModel GetValidUser(string UserName)
        {
            try
            {
                using (var db = new PCAEntities())
                {
                    var user = db.tblUserMasters.FirstOrDefault(x => x.UserName == UserName);
                    if (user != null)
                    {
                        var model = new UserMasterModel();
                        return model.CopyFromDbModel(user);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
            }
            return null;
        }

        /// <summary>
        /// Update last login date on successful login.
        /// </summary>
        /// <param name="UserName"></param>
        public void UpdateSuccessfulLoginDetails(string UserName)
        {
            using (var db = new PCAEntities())
            {
                var user = db.tblUserMasters.FirstOrDefault(x => x.UserName == UserName);
                if (user != null)
                {
                    user.LastLogin = DateTime.Now;
                    user.WrongPasswordCount = 0;
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Wrong password counter.
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="isIncrease"></param>
        public void TryAgainCounter(string UserName)
        {
            using (var db = new PCAEntities())
            {
                var user = db.tblUserMasters.FirstOrDefault(x => x.UserName == UserName);
                if (user != null)
                {
                    user.WrongPasswordCount = user.WrongPasswordCount + 1;
                    if (user.WrongPasswordCount == PassionCardAdmin.Utilities.ConstantVar.MaxWrongPasswordCount)
                    {
                        user.IsBlocked = true;
                    }
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Update password on change password function.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public bool ChangePasswordAdd(UserChangePasswordModel model, string UserName)
        {
            using (var db = new PCAEntities())
            {
                var dbUserModel = db.tblUserMasters.FirstOrDefault(x => x.UserName == UserName.Trim());
                if (dbUserModel != null)
                {
                    if (HashPasswordGenerator.HashPassword(model.CurrentPassword) == dbUserModel.UserPass)
                    {
                        dbUserModel.WrongPasswordCount = 0;

                        if (CheckUserPasswordForLast3Pwds(HashPasswordGenerator.HashPassword(model.NewPassword), dbUserModel))
                        {
                            dbUserModel.Password3 = dbUserModel.Password2;
                            dbUserModel.Password2 = dbUserModel.Password1;
                            dbUserModel.Password1 = dbUserModel.UserPass;
                            dbUserModel.UserPass = HashPasswordGenerator.HashPassword(model.NewPassword);
                            dbUserModel.Password_Change_Date = DateTime.Now;
                            db.SaveChanges();
                        }
                        else
                        {
                            Errors.Add("", "Password must be different from last 3 used password.");
                            return false;
                        }
                    }
                    else
                    {
                        Errors.Add("CurrentPassword", "Old password does not match.");
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// check if new password available in history.
        /// </summary>
        /// <param name="password"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private bool CheckUserPasswordForLast3Pwds(string password, tblUserMaster user)
        {
            var result = true;
            if (user.Password1 == password)
                result = false;
            else if (user.Password2 == password)
                result = false;
            else if (user.Password3 == password)
                result = false;
            else if (user.UserPass == password)
                result = false;

            return result;
        }

    }
}