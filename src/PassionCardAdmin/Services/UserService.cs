﻿using PassionCardAdmin.Helpers;
using PassionCardAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static PassionCardAdmin.Enums.Enum;

namespace PassionCardAdmin.Services
{
    public class UserService : ErrorService
    {
        #region "User region"
        /// <summary>
        /// Returns User list as per filter
        /// </summary>
        /// <param name="formParams"></param>
        /// <returns></returns>
        internal object GetUserList(System.Collections.Specialized.NameValueCollection formParams)
        {
            var form = new GenericListParams(formParams);

            int count = 0;
            decimal totalPages = 0;
            int page = 0;

            using (var db = new PCAEntities())
            {
                var query = db.tblUserMasters.AsQueryable();

                List<UserMasterModel> list = new List<UserMasterModel>();

                var txtUserName = ParamsHelper.GetStringParam(form.Params["UserName"]);

                var sortColumn = ParamsHelper.GetStringParam(form.Params["SortColumn"]);
                var sortOrder = ParamsHelper.GetStringParam(form.Params["SortOrder"]);

                query = GetFilteredQuery(query, sortColumn, sortOrder, txtUserName);

                //calculate total no of pages
                count = query.Count();
                totalPages = ParamsHelper.TotalPages(count, form.Limit);
                page = ParamsHelper.Page(form.Page, totalPages);

                var returnList = query
                    .Skip(form.Start)
                    .Take(form.Limit)
                    .ToList();

                foreach (var model in returnList)
                {
                    var temp = new UserMasterModel();
                    list.Add(temp.CopyFromDbModel(model));
                }

                return new UserListPagingModel
                {
                    page = page,
                    total = totalPages,
                    records = count,
                    data = list
                };
            }
        }

        /// <summary>
        /// Get user by user ID.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        internal tblUserMaster GetCurrentUser(int value)
        {
            using (var db = new PCAEntities())
            {
                var user = db.tblUserMasters.Where(x => x.UserID == value).FirstOrDefault();
                if (user != null)
                {
                    return user;
                }
                else
                {
                    return null;
                }
            }
        }
        /// Unblock User
        internal bool UnblockUser(System.Collections.Specialized.NameValueCollection formparam)
        {
            try
            {
                var form = new GenericListParams(formparam);
                using (var db = new PCAEntities())
                {
                    var uid = ParamsHelper.GetIntParam(form.Params["userid"]);
                    var user = db.tblUserMasters.FirstOrDefault(x => x.UserID == uid);
                    if (user != null)
                    {
                        user.IsBlocked = false;
                        user.WrongPasswordCount = 0;
                        db.SaveChanges();
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception e)
            {
                ExceptionHelper.ExceptionTrack(e);
                return false;
            }
        }
        /// User Activate or Deactivate
        internal bool ActivateDeacivatUser(System.Collections.Specialized.NameValueCollection formparam)
        {
            try
            {
                var form = new GenericListParams(formparam);
                using (var db = new PCAEntities())
                {
                    var userid = ParamsHelper.GetIntParam(form.Params["userid"]);

                    var user = db.tblUserMasters.FirstOrDefault(x => x.UserID == userid);
                    if (user != null)
                    {
                        user.IsDelete = !user.IsDelete;
                        db.SaveChanges();
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
                return false;
            }
        }
        /// <summary>
        /// Check for dupliacte userName.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        internal bool CheckUserName(string userName)
        {
            try
            {
                using (var db = new PCAEntities())
                {
                    var user = db.tblUserMasters.Where(x => x.UserName == userName.Trim().ToString()).FirstOrDefault();
                    if (user != null)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
                return false;
            }
        }

        /// <summary>
        /// Update user details.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        internal bool UpdateUser(CreateUserModel model)
        {
            bool flag = model.IsEdit;
            try
            {
                using (var db = new PCAEntities())
                {
                    var user = db.tblUserMasters.Where(x => x.UserID == model.UserId).FirstOrDefault();
                    if (user != null)
                    {
                        if (flag == true)
                        {
                            user.Name = model.Name;
                            user.UserName = model.UserName;
                        }
                        else
                        {
                            if (user.UserPass != HashPasswordGenerator.HashPassword(model.Password))
                            {
                                if (CheckUserPasswordForLast3Pwds(HashPasswordGenerator.HashPassword(model.Password), user))
                                {
                                    user.Password3 = user.Password2;
                                    user.Password2 = user.Password1;
                                    user.Password1 = user.UserPass;
                                    user.UserPass = HashPasswordGenerator.HashPassword(model.Password);
                                    user.Password_Change_Date = DateTime.Now;
                                }
                                else
                                {
                                    Errors.Add("", "Password must be different from last 3 passwords.");
                                    return false;
                                }
                            }
                        }
                        db.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
                return false;
            }
        }

        /// <summary>
        /// check if new password available in history.
        /// </summary>
        /// <param name="password"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private bool CheckUserPasswordForLast3Pwds(string password, tblUserMaster user)
        {
            var result = true;
            if (user.Password1 == password)
                result = false;
            else if (user.Password2 == password)
                result = false;
            else if (user.Password3 == password)
                result = false;
            else if (user.UserPass == password)
                result = false;

            return result;
        }

        /// <summary>
        /// Add new user with data available in model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        internal bool AddUser(CreateUserModel model)
        {
            try
            {
                using (var db = new PCAEntities())
                {
                    tblUserMaster user = new tblUserMaster()
                    {
                        Name = model.Name,
                        IsDelete = false,
                        Password1 = null,
                        Password2 = null,
                        Password3 = null,
                        LastLogin = DateTime.Now,
                        Password_Change_Date = DateTime.Now,
                        Role = GetUserTypeEnum(model.Role),
                        IsBlocked=false,
                        UserName = model.UserName,
                        UserPass = HashPasswordGenerator.HashPassword(model.Password)
                    };

                    db.tblUserMasters.Add(user);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
                return false;
            }
        }

        private int GetUserTypeEnum(string role)
        {
            UserRole returnRole = (UserRole)Enum.Parse(typeof(UserRole), role);
            return (int)returnRole;
        }

        /// <summary>
        /// filter as per conditions.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <param name="txtUserName"></param>
        /// <returns></returns>
        private IQueryable<tblUserMaster> GetFilteredQuery(IQueryable<tblUserMaster> query, string sortColumn, string sortOrder, string txtUserName)
        {
            //Filter by File Name.
            if (!string.IsNullOrEmpty(txtUserName))
            {
                query = query.Where(x => x.UserName.Contains(txtUserName));
            }

            //Sort by column by Option.
            if (!string.IsNullOrEmpty(sortOrder) && !string.IsNullOrEmpty(sortColumn))
            {
                query = GetSortedColumn(query, sortColumn.ToLower(), sortOrder.ToLower());
            }
            return query;
        }

        /// <summary>
        /// Sort columns as per given options.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <returns></returns>
        private IQueryable<tblUserMaster> GetSortedColumn(IQueryable<tblUserMaster> query, string sortColumn, string sortOrder)
        {
            switch (sortColumn.ToLower())
            {
                case "user id":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.UserID);
                    else
                        return query.OrderByDescending(x => x.UserID);

                case "name":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.Name);
                    else
                        return query.OrderByDescending(x => x.Name);

                case "username":
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.UserName);
                    else
                        return query.OrderByDescending(x => x.UserName);

                default:
                    if (sortOrder == "asc")
                        return query.OrderBy(x => x.UserName);
                    else
                        return query.OrderByDescending(x => x.UserName);
            }
        }

        #endregion
    }
}