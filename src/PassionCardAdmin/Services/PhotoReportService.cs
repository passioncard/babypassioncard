﻿using Microsoft.Reporting.WebForms;
using PassionCardAdmin.Helpers;
using PassionCardAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PassionCardAdmin.Services
{
    public class PhotoReportService : ErrorService
    {
        public PhotoReportService()
        {

        }

        /// <summary>
        /// Gets Photo Report To show data on view.
        /// </summary>
        /// <param name="formParams"></param>
        /// <returns></returns>
        internal object GetPhotoReport(FormCollection formParams)
        {
            var form = new GenericListParams(formParams);

            int count = 0;
            decimal totalPages = 0;
            int page = 0;

            using (var db = new PCAEntities())
            {
                var query = db.tblPhotoResetHistories.AsQueryable();

                List<PhotoResetDataModel> list = new List<PhotoResetDataModel>();
                var FromDate = ParamsHelper.GetNullableDate(form.Params["FromDate"]);
                var ToDate = ParamsHelper.GetNullableDate(form.Params["ToDate"]);
                var ICNumber = ParamsHelper.GetStringParam(form.Params["IDNumber"]);

                //Filter by fromDate
                if (FromDate.HasValue && FromDate.Value != DateTime.MinValue)
                {
                    var date = FromDate.Value.Date;
                    query = query.Where(x => x.CreatedDate >= date);
                }

                //Filter by toDate
                if (ToDate.HasValue && ToDate.Value != DateTime.MinValue)
                {
                    var date = ToDate.Value.Date.AddDays(1);
                    query = query.Where(x => x.CreatedDate <= date);
                }

                if(!string.IsNullOrEmpty(ICNumber))
                {
                    query = query.Where(x => x.IDNumber == ICNumber);
                }

                ////calculate total no of pages
                count = query.Count();
                totalPages = ParamsHelper.TotalPages(count, form.Limit);
                page = ParamsHelper.Page(form.Page, totalPages);

                query = query
                    .OrderBy(x => x.Id)
                    .Skip(form.Start)
                    .Take(form.Limit);
                int srNo = form.Start;

                List<PhotoResetDataModel> returnList = new List<PhotoResetDataModel>();
                var groups= db.tblPhotoResetHistories.AsQueryable().GroupBy(x => x.IDNumber);

                foreach (var item in query)
                {
                    var mod = new PhotoResetDataModel().FromDbModel(item, ++srNo);
                    var group = groups.FirstOrDefault(x=>x.Key == item.IDNumber);
                    mod.IsHighlight = GetHighlightStatus(group.OrderBy(x=>x.CreatedDate).ToList());

                    returnList.Add(mod);
                }

                return new PhotoResetDataPagingModel
                {
                    page = page,
                    total = totalPages,
                    records = count,
                    data = returnList
                };
            }
        }

        /// <summary>
        /// Return true or false depending on is it created in same year.
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private bool GetHighlightStatus(List<tblPhotoResetHistory> list)
        {
            if (list.Count() >= 3)
            {
                var startDate = list[list.Count() - 3].CreatedDate.Date;
                var endDate = startDate.AddYears(1);

                var newList = list.Where(x=>x.CreatedDate >= startDate.Date && x.CreatedDate<= endDate.Date);
                if (newList.Count() >= 3)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Generates photo report 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="IsPdf"></param>
        /// <param name="response"></param>
        /// <param name="server"></param>
        /// <returns></returns>
        public bool ExportToReport(PhotoResetReportModel model, bool IsPDF, HttpResponseBase response, HttpServerUtilityBase server)
        {
            string FromToDateString = model.FromDate.ToString("dd MMM yyyy") + " To " + ((model.ToDate == DateTime.MinValue) ? DateTime.MaxValue.ToString("dd MMM yyyy") : model.ToDate.ToString("dd MMM yyyy"));
            string TodaysDate = DateTime.Now.ToString("dd MMM yyyy");
            string PrintedBy = Utilities.ConstantVar.CurrentUserName;

            LocalReport localReport = new LocalReport();

            localReport.ReportPath = server.MapPath(Utilities.ConstantVar.ReportPath + "PrintResetReport.rdlc");

            //Passing Parameters                
            localReport.SetParameters(new ReportParameter("FromToDateString", FromToDateString));
            localReport.SetParameters(new ReportParameter("TodaysDate", TodaysDate));
            localReport.SetParameters(new ReportParameter("PrintedBy", PrintedBy));

            ReportDataSource reportDataSource = new ReportDataSource("PhotoResetReportDataset", GetPhotoReportData(model));
            localReport.DataSources.Clear();
            localReport.DataSources.Add(reportDataSource);

            localReport.Refresh();

            string extensionString = "xls";
            string outputType = "Excel";

            if (IsPDF)
            {
                extensionString = "Pdf";
                outputType = "Pdf";
            }

            ExportReportHelper.ReportExportHelper(localReport, outputType, "PhotoResetReport", true, extensionString, response);

            return false; 
        }

        /// <summary>
        /// Returns list of Photo that have been reseted.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private List<PhotoResetDataModel> GetPhotoReportData(PhotoResetReportModel model)
        {
            using (var db = new PCAEntities())
            {
                List<PhotoResetDataModel> returnList = new List<PhotoResetDataModel>();

                var query = db.tblPhotoResetHistories.AsQueryable();

                List<PhotoResetDataModel> list = new List<PhotoResetDataModel>();

                //Filter by fromDate
                if (model.FromDate != DateTime.MinValue)
                {
                    query = query.Where(x => x.CreatedDate >= model.FromDate);
                }

                //Filter by toDate
                if (model.ToDate != DateTime.MinValue)
                {
                    var date = model.ToDate.AddDays(1);
                    query = query.Where(x => x.CreatedDate <= date);
                }

                if (!string.IsNullOrEmpty(model.ICNumber))
                {
                    query = query.Where(x => x.IDNumber == model.ICNumber);
                }

                var groups = db.tblPhotoResetHistories.ToList().GroupBy(x=>x.IDNumber);
                int srNo = 0;
                foreach (var item in query)
                {
                    var mod = new PhotoResetDataModel().FromDbModel(item, ++srNo);                    
                    var group = groups.FirstOrDefault(x => x.Key == item.IDNumber);
                    mod.IsHighlight = GetHighlightStatus(group.OrderBy(x => x.CreatedDate).ToList());

                    returnList.Add(mod);
                }

                return returnList;
            }
        }
    }
}