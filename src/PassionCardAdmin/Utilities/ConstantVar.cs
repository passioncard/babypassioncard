﻿using PassionCardAdmin.Helpers;
using PassionCardAdmin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using static PassionCardAdmin.Enums.Enum;

namespace PassionCardAdmin.Utilities
{
    public static class ConstantVar
    {
        public const int PACARDLength = 805;
        public const int SACARDLength = 821;
        //If wrong password count is exceeded this count then user is blocked.
        public const int MaxWrongPasswordCount = 5;
        public const string AdminUserRoles = "Admin";

        /// <summary>
        /// get user name
        /// </summary>
        public static string CurrentUserName
        {
            get
            {
                string userName = string.Empty;

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    userName = GetInfo(IdentityType.UserName, HttpContext.Current.User.Identity.Name);
                }
                else
                {
                    var requestContext = HttpContext.Current.Request.RequestContext;
                    HttpContext.Current.Response.Buffer = true;
                    HttpContext.Current.Response.Redirect(new System.Web.Mvc.UrlHelper(requestContext).Action("Login", "Account"));
                }

                return userName;
            }
        }

        /// <summary>
        /// Get Current session CurrentUserId
        /// </summary>
        public static int CurrentUserId
        {
            get
            {
                string userIdStr = string.Empty;
                int userid = 0;
                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    userIdStr = GetInfo(IdentityType.UserId, HttpContext.Current.User.Identity.Name);
                    int.TryParse(userIdStr, out userid);
                }
                else
                {
                    var requestContext = HttpContext.Current.Request.RequestContext;
                    HttpContext.Current.Response.Buffer = true;
                    HttpContext.Current.Response.Redirect(new System.Web.Mvc.UrlHelper(requestContext).Action("Login", "Account"));
                }

                return userid;
            }
        }         

        public static UserRole CurrentUserType
        {
            get
            {
                UserRole userName = UserRole.None;

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    Enum.TryParse(GetInfo(IdentityType.UserType, HttpContext.Current.User.Identity.Name), out userName);
                }
                else
                {
                    var requestContext = HttpContext.Current.Request.RequestContext;
                    HttpContext.Current.Response.Buffer = true;
                    HttpContext.Current.Response.Redirect(new System.Web.Mvc.UrlHelper(requestContext).Action("Login", "Account"));
                }

                return userName;
            }
        }

        public static string LastLoginTime
        {
            get
            {
                string LastLogin = "";

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    LastLogin = GetInfo(IdentityType.LastLogin, HttpContext.Current.User.Identity.Name);
                }
                else
                {
                    var requestContext = HttpContext.Current.Request.RequestContext;
                    HttpContext.Current.Response.Buffer = true;
                    HttpContext.Current.Response.Redirect(new System.Web.Mvc.UrlHelper(requestContext).Action("Login", "Account"));
                }

                return LastLogin;
            }
        }


        public static bool IsPasswordExpired
        {
            get
            {
                bool IsPasswordExpired = false;

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    IsPasswordExpired = Convert.ToBoolean(GetInfo(IdentityType.IsPasswordExpired, HttpContext.Current.User.Identity.Name));
                }
                else
                {
                    var requestContext = HttpContext.Current.Request.RequestContext;
                    HttpContext.Current.Response.Buffer = true;
                    HttpContext.Current.Response.Redirect(new System.Web.Mvc.UrlHelper(requestContext).Action("Login", "Account"));
                }

                return (IsPasswordExpired);
            }
        }


        public static string GetInfo(IdentityType type, string str)
        {
            try
            {
                var dataArray = str.Split(',');
                foreach (var item in dataArray)
                {
                    var dataDetails = item.Split(':');
                    if (dataDetails.Count() == 2 && dataDetails[0] == type.ToString())
                    {
                        return dataDetails[1];
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                ExceptionHelper.ExceptionTrack(ex);
                return null;
            }

        }

        public static string GetINameString(UserMasterModel user)
        {
            StringBuilder str = new StringBuilder();
            str.Append(PassionCardAdmin.Enums.Enum.IdentityType.UserId.ToString() + ":" + user.UserID + ",");
            str.Append(PassionCardAdmin.Enums.Enum.IdentityType.UserName.ToString() + ":" + user.UserName + ",");
            str.Append(PassionCardAdmin.Enums.Enum.IdentityType.UserType.ToString() + ":" + (PassionCardAdmin.Enums.Enum.UserRole)user.Role + ",");
            str.Append(Enums.Enum.IdentityType.LastLogin.ToString() + ":" + user.LastLoginDateTimeString + ",");
            if (user.Password_Change_Date.Value.AddMonths(1).Date < DateTime.Now.Date)
            {
                str.Append(Enums.Enum.IdentityType.IsPasswordExpired.ToString() + ":" + true);
            }
            else
            {
                str.Append(Enums.Enum.IdentityType.IsPasswordExpired.ToString() + ":" + false);
            }

            return str.ToString();
        }


        public static string SummaryImageFolder = "ReportImagesToSave";

        public static string ReportPath = "~/bin/Reports/";
        
        public static string BaseUrl(HttpRequestBase Request)
        {
            var url = Request.Url.Scheme +
                    "://" +
                    Request.Url.Authority +
                    Request.ApplicationPath.TrimEnd('/') +
                    '/';

            return url;
        }
    }
}