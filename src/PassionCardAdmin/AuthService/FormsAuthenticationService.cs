﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace PassionCardAdmin.AuthService
{
    public class FormsAuthenticationService : IAuthenticationService
    {
        public FormsAuthenticationService()
        {
        }

        public string DefaultRedirectUrl
        {
            get { return "/Account/Login"; }
        }

        public void Login(string userId, string roles, bool rememberMe = false)
        {
            var expire = DateTime.Now.AddDays(1);
            if (rememberMe)
                expire = DateTime.Now.AddDays(14);

            var ticket = new FormsAuthenticationTicket(
                1,
                userId,
                DateTime.Now,
                expire,
                rememberMe,
                roles
            );
            var rawCookie = FormsAuthentication.Encrypt(ticket);
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, rawCookie);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public void Logoff()
        {
            FormsAuthentication.SignOut();
        }

        public IPrincipal ProcessRequest(string ticketName)
        {
            IPrincipal principal = null;

            if (!string.IsNullOrEmpty(ticketName))
            {
                try
                {
                    var identity = new GenericIdentity(ticketName, "Forms");
                    principal = new GenericPrincipal(identity, new string[] { });
                }
                catch
                { }
            }

            return principal;
        }
    }
}