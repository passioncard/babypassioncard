﻿// -----------------------------------------------------------------------
// <copyright file="IAuthenticationService" company="Manoj Patil">
// Copyright (c) 2017 All Rights Reserved
// </copyright>
// <author>Manoj Patil</author>
// <date>13/02/2017 3:07:22 PM</date>
// <summary></summary>
// -----------------------------------------------------------------------

namespace PassionCardAdmin.AuthService
{
    using System.Security.Principal;

    interface IAuthenticationService
    {
        string DefaultRedirectUrl { get; }
        void Login(string userId, string roles, bool rememberMe = false);
        void Logoff();
        IPrincipal ProcessRequest(string ticketName);
    }
}
