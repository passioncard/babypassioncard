﻿using System.Web;
using System.Web.Optimization;

namespace PassionCardAdmin.App_Start
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Content/plugins/jQuery/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Content/plugins/jQueryUI/jquery-ui-{version}.js"));


            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                  "~/Content/bootstrap/js/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/commoncss").Include(
              "~/Content/bootstrap/css/bootstrap.css",
              "~/Content/bootstrap/css/AdminLTE.css",
              "~/Content/bootstrap/css/skins/_all-skins.css",
              "~/Content/bootstrap/css/font-awesome.css",
              "~/Content/bootstrap/css/ionicons.css",
              "~/Content/bootstrap/css/common-style.css",
              "~/Content/plugins/datepicker/datepicker3.css",
              "~/Content/CardPreview.css",
              "~/Content/jquery.notify.css"
              ));

            bundles.Add(new StyleBundle("~/bundles/commonDateRangeCss").Include(
                "~/Content/plugins/daterangepicker/daterangepicker.css"
                ));

            bundles.Add(new StyleBundle("~/bundles/commonDateTableCss").Include(
               "~/Content/plugins/datatables/dataTables.bootstrap.css"
               ));

            bundles.Add(new ScriptBundle("~/bundles/commonjs").Include(
                "~/Content/plugins/jQuery/jquery-2.2.3.js",
                 "~/Content/bootstrap/js/bootstrap.js",
                 "~/Content/bootstrap/js/app.js",
                 "~/Content/bootstrap/js/jquery.validate.js",
                 "~/Content/bootstrap/js/jquery.validate.unobtrusive.min",
                 "~/Content/jquery.ui.widget.js",
                  "~/Content/jquery.notify.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/commonDateRangejs").Include(
                "~/Content/plugins/daterangepicker/moment.js",
                "~/Content/plugins/daterangepicker/daterangepicker.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/commonDatejs").Include(
                 "~/Content/plugins/datepicker/bootstrap-datepicker.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/commonDateTablejs").Include(
                 "~/Content/plugins/datatables/jquery.dataTables.js",
                  "~/Content/plugins/datatables/dataTables.bootstrap.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/TypeHeadjs").Include(
               "~/Content/bootstrap/js/Bootstrap3-typehead.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/commonChartjs").Include(
               "~/Content/plugins/chartjs/Chart.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/treejs").Include(
                        "~/Content/plugins/TreeGrid/jquery.treegrid.js",
                        "~/Content/plugins/TreeGrid/jquery.treegrid.bootstrap3.js"));

            bundles.Add(new StyleBundle("~/bundles/treecss").Include(
               "~/Content/plugins/TreeGrid/jquery.treegrid.css"
               ));

            bundles.Add(new ScriptBundle("~/bundles/inputmaskjs").Include(
                "~/Content/plugin/input-mask/jquery.inputmask.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/paginationjs").Include(
              "~/Content/bootstrap/js/jquery.bootpag.min.js"
            ));           
        }
    }
}