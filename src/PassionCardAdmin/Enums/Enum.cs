﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionCardAdmin.Enums
{
    public class Enum
    {

        public enum UserRole
        {
            None,
            Admin,
            User
        }

        /// <summary>
        /// to specify file type
        /// </summary>
        public enum FileTypes
        {
            Import,
            ExcelExport,
            ACKExport
        }


        public enum IdentityType
        {
            UserId,
            UserName,
            UserType,
            LastLogin,
            IsPasswordExpired
        }

        /// <summary>
        /// No         => Default N  
        /// Yes        => When picture gets uploaded update value to Y  
        /// Print      => When sent to print file then it again P  
        /// CANUpdated => When CAN value update change to U  
        /// </summary>
        public enum SendInPrintFile
        {
            No,
            Yes,
            Print,
            CANUpdated
        }


        /// <summary>
        /// Default Value 'N'
        /// PR = Picture Received
        /// SP = Sent to Print
        /// PC = Printing Completed
        /// RD = Card Ready for Dispatch
        /// </summary>
        public enum RecordStatus
        {
            N,
            PR,
            SP,
            PC,
            RD
        }

        public enum FileType
        {
            SACARD,
            PACARD
        }

        public enum UserStatus
        {
            InActive=0,
            Active            
        }
    }
}