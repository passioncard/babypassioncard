﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionCardAdmin.Models
{
    public class SummaryDetail
    {
        //title will be date for month filter and weekdays for Week
        public string Title { get; set; }

        //saqle value is the sum for day wise
        public double PictureCountValue { get; set; }
    }
}