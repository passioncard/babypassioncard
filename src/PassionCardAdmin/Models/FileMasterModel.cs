﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static PassionCardAdmin.Enums.Enum;

namespace PassionCardAdmin.Models
{
    public class FileMasterModel
    {
        public int RecordID { get; set; }
        public string HeaderFileID { get; set; }
        public System.DateTime? HeaderFileCreationDate { get; set; }
        public string FooterFileID { get; set; }
        public int FooterRecordTotals { get; set; }
        public string FileName { get; set; }
        public System.DateTime RecordCreatedDate { get; set; }
        public int FileType { get; set; }
        public int UserID { get; set; }
        public string FileTypreStr { get; set; }
        public string RecordCreatedDateStr { get; set; }

        public FileMasterModel CopyFromDbModel(tblFileMaster fileEntity)
        {
            return new FileMasterModel
            {
                FileName = fileEntity.FileName,
                FileType = fileEntity.FileType,
                FileTypreStr = ((FileTypes)fileEntity.FileType).ToString(),
                FooterFileID = string.IsNullOrEmpty(fileEntity.FooterFileID) ? "" : fileEntity.FooterFileID,
                FooterRecordTotals = fileEntity.FooterRecordTotals.HasValue ? fileEntity.FooterRecordTotals.Value : 0,
                HeaderFileCreationDate = fileEntity.HeaderFileCreationDate,
                HeaderFileID = string.IsNullOrEmpty(fileEntity.HeaderFileID) ? "" : fileEntity.HeaderFileID,
                RecordCreatedDate = fileEntity.RecordCreatedDate,
                RecordID = fileEntity.RecordID,
                UserID = fileEntity.UserID,
                RecordCreatedDateStr = fileEntity.RecordCreatedDate.ToString("dd-MMM-yyyy")
            };
        }
    }

    public class FileTypeModel
    {
        public string Text { get; set; }
        public FileType Value { get; set; }
    }

    public class FileListPagingModel
    {
        public int page { get; set; }
        public int records { get; set; }
        public decimal total { get; set; }
        public List<FileMasterModel> data { get; set; }

        public FileListPagingModel()
        {
            data = new List<FileMasterModel>();
        }
    }
}