﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionCardAdmin.Models
{
    public class CardTappingModel
    {
        public int RecordID { get; set; }
        public string NameOnCard { get; set; }//Name
        public string IDNumber { get; set; }
        public decimal PAssionCardNumber { get; set; }
        public string PAssionCardNumberStr { get; set; }//card number
        public string Gender { get; set; }
        public string PrintBU { get; set; }//Print BU

        /// <summary>
        ///  define from gender 000 : Invalid , 001 : Female,  002 : Male
        /// </summary>
        public string GenderStr
        {
            get
            {
                string returnString = string.Empty;
                switch (Gender)
                {
                    case "000":
                        returnString = "Invalid";
                        break;
                    case "001":
                        returnString = "Female";
                        break;
                    case "002":
                        returnString = "Male";
                        break;
                }
                return returnString;
            }
        }

        public decimal CardCANNumber { get; set; }
        public string CardCANExpiryDt { get; set; }//Expiry date
    }
}