﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using static PassionCardAdmin.Enums.Enum;

namespace PassionCardAdmin.Models
{
    public class UserMasterModel
    {
        public int UserID { get; set; }

        [Required(ErrorMessage = "Please enter USERNAME")]
        [DisplayName("USERNAME")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter PASSWORD")]
        [DataType(DataType.Password)]
        [DisplayName("PASSWORD")]
        [MaxLength(20)]
        public string UserPass { get; set; }

        public bool IsDelete { get; set; }
        public System.DateTime LastLogin { get; set; }
        public Enums.Enum.UserRole Role { get; set; }

        public string Name { get; set; }
        public string Password1 { get; set; }
        public string Password2 { get; set; }
        public string Password3 { get; set; }
        public Nullable<System.DateTime> Password_Change_Date { get; set; }
        public Enums.Enum.UserStatus Status { get; set; }
        public string LastLoginStr { get; set; }
        public string StatusStr { get; set; }
        public string RoleStr { get; set; }
        public string LastLoginDateTimeString { get; set; }
        public int WrongPasswordCount { get; set; }
        public bool IsBlocked { get; set; }

        /// <summary>
        /// create model from entity
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public UserMasterModel CopyFromDbModel(tblUserMaster model)
        {
            return new UserMasterModel()
            {
                UserID = model.UserID,
                UserName = model.UserName,
                UserPass = model.UserPass,
                IsDelete = model.IsDelete,
                LastLogin = model.LastLogin,
                Role = (Enums.Enum.UserRole)model.Role,
                Name = model.Name,
                Password1 = model.Password1,
                Password2 = model.Password2,
                Password3 = model.Password3,
                Password_Change_Date = model.Password_Change_Date,
                LastLoginStr = model.LastLogin.ToShortDateString(),
                Status = (model.IsDelete) ? Enums.Enum.UserStatus.Active : Enums.Enum.UserStatus.InActive,
                StatusStr = (model.IsDelete) ? Enums.Enum.UserStatus.InActive.ToString() : Enums.Enum.UserStatus.Active.ToString(),
                RoleStr = ((Enums.Enum.UserRole)model.Role).ToString(),
                LastLoginDateTimeString = model.LastLogin.ToShortDateString(),
                IsBlocked= (model.IsBlocked.HasValue)?model.IsBlocked.Value:false,
                WrongPasswordCount = model.WrongPasswordCount.HasValue ? model.WrongPasswordCount.Value : 0
            };
        }
    }

    public class UserListPagingModel
    {
        public int page { get; set; }
        public int records { get; set; }
        public decimal total { get; set; }
        public List<UserMasterModel> data { get; set; }

        public UserListPagingModel()
        {
            data = new List<UserMasterModel>();
        }
    }

    public class CreateUserModel
    {
        public int UserId { get; set; }

        [Required(ErrorMessage = "Please enter USERNAME")]
        [DisplayName("USERNAME")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter NAME")]
        [DisplayName("NAME")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter PASSWORD")]
        [DisplayName("PASSWORD")]
        [RegularExpression(@"(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#%*()$?+-=^]).{12,20}", ErrorMessage = "Password must contain at least 12 characters including 1 Uppercase , 1 Lowercase, 1 Number and 1 special character")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please enter CONFIRM PASSWORD")]
        [DisplayName("CONFIRM PASSWORD")]
        [RegularExpression(@"(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#%*()$?+-=^]).{12,20}", ErrorMessage = "Password must contain at least 12 characters including 1 Uppercase , 1 Lowercase, 1 Number and 1 special character")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "New password and Confirm new password did not match")]
        public string ConfirmPassword { get; set; }

        public string Role { get; set; }

        public bool IsEdit { get; set; }

        public CreateUserModel CopyFromUserModel(tblUserMaster uMaster)
        {
            return new CreateUserModel
            {
                UserId = uMaster.UserID,
                Name = uMaster.Name,
                Password = "",
                ConfirmPassword = "",
                UserName = uMaster.UserName
            };
        }
    }

    public class UserChangePasswordModel
    {
        [Required(ErrorMessage = "Please enter Current Password")]
        [DataType(DataType.Password)]
        [DisplayName("CURRENT PASSWORD")]
        public string CurrentPassword { get; set; }

        [Required(ErrorMessage = "Please enter New Password")]
        [RegularExpression(@"(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#%*()$?+-=^]).{12,20}", ErrorMessage = "Password must contain at least 12 characters including 1 Uppercase , 1 Lowercase, 1 Number and 1 special character")]
        [DataType(DataType.Password)]
        [DisplayName("NEW PASSWORD")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Please enter Confirm new Password")]
        [RegularExpression(@"(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#%*()$?+-=^]).{12,20}", ErrorMessage = "Password must contain at least 12 characters including 1 Uppercase , 1 Lowercase, 1 Number and 1 special character")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "New password and Confirm new password did not match")]
        [DisplayName("CONFIRM NEW PASSWORD")]
        public string ConfirmPassword { get; set; }
    }

    public class UserProfileModel
    {
        public UserMasterModel userModel { get; set; }

        public UserProfileModel()
        {
            userModel = new UserMasterModel();
        }
    }

    public class UserTypeModel
    {
        public string Text { get; set; }
        public UserRole Value { get; set; }
    }
}