﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionCardAdmin.Models
{
    public class CoBrandedMasterModel
    {
        public string CoBrandedValue { get; set; }
        public string Code { get; set; }
        public int RecordID { get; set; }
    }
}