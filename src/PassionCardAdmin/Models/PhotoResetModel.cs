﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PassionCardAdmin.Models
{
    public class PhotoResetModel
    {
        [Required]
        [MaxLength(10)]
        public string ICNumber { get; set; }

        public bool isShowMessage { get; set; }
    }

    public class PhotoResetReportModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string ICNumber { get; set; }
    }

    public class PhotoResetDataModel
    {
        public int SrNo { get; set; }
        public string ICNumber { get; set; }
        public string Name { get; set; }
        public string Admin { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedDateStr { get; set; }
        public bool IsHighlight { get; set; }

        public PhotoResetDataModel FromDbModel(tblPhotoResetHistory entity, int Id)
        {
            return new PhotoResetDataModel()
            {
                Admin = entity.CreatedByName,
                CreatedDate = entity.CreatedDate,
                ICNumber = entity.IDNumber,
                Name = entity.Name,
                SrNo = Id,
                CreatedDateStr = entity.CreatedDate.ToString("dd-MMM-yyyy")
            };
        }
    }

    public class PhotoResetDataPagingModel
    {
        public int page { get; set; }
        public int records { get; set; }
        public decimal total { get; set; }
        public string Title { get; set; }
        public List<PhotoResetDataModel> data { get; set; }

        public PhotoResetDataPagingModel()
        {
            data = new List<PhotoResetDataModel>();
        }
    }
}