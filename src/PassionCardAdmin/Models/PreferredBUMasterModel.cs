﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionCardAdmin.Models
{
    public class PreferredBUMasterModel
    {
        public string PreferredBUValue { get; set; }
        public string Code { get; set; }
        public int RecordID { get; set; }
    }
}