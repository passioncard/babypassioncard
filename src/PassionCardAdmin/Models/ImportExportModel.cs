﻿using PassionCardAdmin.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionCardAdmin.Models
{
     

    public class ExportToXlsModel
    {
        public int SrNo { get; set; }
        public int RecordId { get; set; }
        public string ActionFlags { get; set; }
        public string NameOnCard { get; set; }
        public decimal PAssionCardNumber { get; set; }
        public string PAssionCardNumberStrWSP { get { return PAssionCardNumber.ToString().ToPMembership(); } }
        public string PAssionCardNumberStr { get { return PAssionCardNumber.ToString(); } }
        public string IDNumber { get; set; }
        /// <summary>
        /// number star and end with *
        /// </summary>
        public string IDNumber1 { get { return string.IsNullOrEmpty(IDNumber) ? "" : "*" + IDNumber + "*"; } }

        public System.DateTime ExpiryDate { get; set; }
        //format like 31-Dec-2020
        public string ExpiryDateStr { get; set; }

        public string ExpiryDtYYMM
        {
            get
            {
                return ExpiryDate.ToString("yyMM");
            }
        }

        public string HA_BlockHouseNo { get; set; }
        public string HA_StreetName { get; set; }
        public string Address1
        {
            get { return HA_BlockHouseNo + (string.IsNullOrEmpty(HA_StreetName) ? "" : "," + HA_StreetName); }
        }

        public string HA_FloorNo { get; set; }
        public string HA_UnitNo { get; set; }
        public string HA_BuildingName { get; set; }
        public string Address2
        {
            get { return "#" + (string.IsNullOrEmpty(HA_FloorNo) ? "" : HA_FloorNo) + (string.IsNullOrEmpty(HA_UnitNo) ? "" : "-" + HA_UnitNo) + (string.IsNullOrEmpty(HA_BuildingName) ? "" : "," + HA_BuildingName); }
        }

        public string HA_PostalCode { get; set; }

        public string Gender { get; set; }

        /// <summary>
        ///  define from gender 000 : Invalid , 001 : Female,  002 : Male
        /// </summary>
        public string GenderStr
        {
            get
            {
                string returnString = string.Empty;
                switch (Gender)
                {
                    case "000":
                        returnString = "Invalid";
                        break;
                    case "001":
                        returnString = "Female";
                        break;
                    case "002":
                        returnString = "Male";
                        break;
                }
                return returnString;
            }
        }

        /// <summary>
        /// define from gender 000 : Black , 001 : Pink,  002 : Blue
        /// </summary>
        public string CardTypeStr
        {
            get
            {
                string returnString = string.Empty;
                switch (Gender)
                {
                    case "000":
                        returnString = "Black";
                        break;
                    case "001":
                        returnString = "Pink";
                        break;
                    case "002":
                        returnString = "Blue";
                        break;
                }
                return returnString;
            }
        }

        public string PrintBU { get; set; }
        public string PrintBUStr { get; set; }
     

        /// <summary>
        /// TODO : manoj : required information
        /// </summary>
        public string Code { get; set; }

        public string CoBrandedCardType { get; set; }
        public string CoBrandedCardTypeStr { get; set; }


        public string WV { get; set; }
        public string PAYM { get; set; }
        public string CoBranded { get; set; }



        #region ACK file 
        public decimal CANNumber { get; set; }
        public string CanNumberStr { get { return CANNumber.ToString(); } }

        public string ReturnCode { get; set; }
        public string ReturnMessage { get; set; }
        public string OldPassionezLinkNumber { get; set; }
        #endregion


    }

}