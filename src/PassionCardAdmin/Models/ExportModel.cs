﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionCardAdmin.Models
{
    public class ExportModel
    {
        public int SrNum { get; set; }
        public bool IsChecked { get; set; }
        public int RecordID { get; set; }
        public string NameOnCard { get; set; }
        public string IDNumber { get; set; }
        public decimal PAssionCardNumber { get; set; }
        public string Gender { get; set; }
    }

    public class ExportModelList
    {
        public List<ExportModel> MainExportModel { get; set; }
    }
}