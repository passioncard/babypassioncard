﻿using PassionCardAdmin.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PassionCardAdmin.Models
{
    public class CardMasterModel
    {
        public int RecordID { get; set; }
        public string ActionFlags { get; set; }
        public string Name { get; set; }
        public string NameOnCard { get; set; }
        public string IDType { get; set; }
        public string IDNumber { get; set; }
        public decimal PAssionCardNumber { get; set; }
        public string PreferredBU { get; set; }
        public string EmailAddress { get; set; }
        public string MA_BlockHouseNo { get; set; }
        public string MA_StreetName { get; set; }
        public string MA_FloorNo { get; set; }
        public string MA_UnitNo { get; set; }
        public string MA_BuildingName { get; set; }
        public string MA_PostalCode { get; set; }
        public string HA_BlockHouseNo { get; set; }
        public string HA_StreetName { get; set; }
        public string HA_FloorNo { get; set; }
        public string HA_UnitNo { get; set; }
        public string HA_BuildingName { get; set; }
        public string HA_PostalCode { get; set; }
        public System.DateTime ExpiryDate { get; set; }
        public string HpNumber { get; set; }
        public string ContactNumberHome { get; set; }
        public string WV { get; set; }
        public string PAYM { get; set; }
        public string CoBranded { get; set; }
        public string CoBrandedCardType { get; set; }
        public string CardTypeFlag { get; set; }
        public System.DateTime Birthday { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }
        public string AnnualIncome { get; set; }
        public string HousingType { get; set; }
        public string HighestEducationLevel { get; set; }
        public string PrintBU { get; set; }
        public string PreferredWV { get; set; }
        public string BillingBU { get; set; }
        public string OldPassionezLinkNumber { get; set; }
        public string Nationality { get; set; }
        public string Title { get; set; }
        public string Race { get; set; }
        public string Occupation { get; set; }
        public int UserId { get; set; }
        public int FileMasterId { get; set; }
        public System.DateTime RecordCreatedDt { get; set; }
        public System.DateTime RecordModifiedDt { get; set; }
        public bool PictureUpdated { get; set; }
        public string PictureFile { get; set; }
        public byte[] Picture { get; set; }
        public int SendInPrintFile { get; set; }
        public int PrintFileRefId { get; set; }
        public decimal CardCANNumber { get; set; }
        public string CardCANExpiryDt { get; set; }
        public bool SendAck { get; set; }
        public int AckFileRefId { get; set; }
        public string RecordStatus { get; set; }
        public Nullable<System.DateTime> RecordStatusUpdateDt { get; set; }

        public string ExpiryDateStr { get; set; }
        public string BirthdayStr { get; set; }
        public string RecordCreatedDtStr { get; set; }
        public string RecordModifiedDtStr { get; set; }
        public string RecordStatusUpdateDtStr { get; set; }
        public string PreferedBUStr { get; set; }
        public string CobrandedTypeStr { get; set; }
        public int SrNo { get; set; }

        /// <summary>
        /// database entity to model
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        internal CardMasterModel CopyFromDbToModel(tblCardMaster entity, string PBU = null, string CBCT = null, int srNo = 0)
        {
            return new CardMasterModel()
            {
                RecordID = entity.RecordID,
                ActionFlags = entity.ActionFlags,

                SrNo=srNo,
                Name = entity.Name,
                NameOnCard = entity.NameOnCard,
                IDType = entity.IDType,
                IDNumber = entity.IDNumber,
                PAssionCardNumber = entity.PAssionCardNumber,
                PreferredBU = entity.PreferredBU,
                EmailAddress = entity.EmailAddress,
                MA_BlockHouseNo = entity.MA_BlockHouseNo,
                MA_StreetName = entity.MA_StreetName,
                MA_FloorNo = entity.MA_FloorNo,
                MA_UnitNo = entity.MA_UnitNo,
                MA_BuildingName = entity.MA_BuildingName,
                MA_PostalCode = entity.MA_PostalCode,
                HA_BlockHouseNo = entity.HA_BlockHouseNo,
                HA_StreetName = entity.HA_StreetName,
                HA_FloorNo = entity.HA_FloorNo,
                HA_UnitNo = entity.HA_UnitNo,
                HA_BuildingName = entity.HA_BuildingName,
                HA_PostalCode = entity.HA_PostalCode,
                ExpiryDate = entity.ExpiryDate,
                HpNumber = entity.HpNumber,
                ContactNumberHome = entity.ContactNumberHome,
                WV = entity.WV,
                PAYM = entity.PAYM,
                CoBranded = entity.CoBranded,
                CoBrandedCardType = entity.CoBrandedCardType,
                CardTypeFlag = entity.CardTypeFlag,
                Birthday = entity.Birthday,
                Gender = entity.Gender,
                MaritalStatus = entity.MaritalStatus,
                AnnualIncome = entity.AnnualIncome,
                HousingType = entity.HousingType,
                HighestEducationLevel = entity.HighestEducationLevel,
                PrintBU = entity.PrintBU,
                PreferredWV = entity.PreferredWV,
                BillingBU = entity.BillingBU,
                OldPassionezLinkNumber = entity.OldPassionezLinkNumber,
                Nationality = entity.Nationality,
                Title = entity.Title,
                Race = entity.Race,
                Occupation = entity.Occupation,
                UserId = entity.UserId,
                FileMasterId = entity.FileMasterId,
                RecordCreatedDt = entity.RecordCreatedDt,
                RecordModifiedDt = entity.RecordModifiedDt,

                PictureUpdated = entity.PictureUpdated,
                PictureFile = entity.PictureFile.IsStringNullCHeck(),
                //Picture = entity.Picture,
                SendInPrintFile = entity.SendInPrintFile,
                PrintFileRefId = entity.PrintFileRefId,
                CardCANNumber = entity.CardCANNumber,
                CardCANExpiryDt = entity.CardCANExpiryDt,
                SendAck = entity.SendAck,
                AckFileRefId = entity.AckFileRefId,
                RecordStatus = entity.RecordStatus,
                RecordStatusUpdateDt = entity.RecordStatusUpdateDt,

                BirthdayStr = entity.Birthday.ToString("dd/MM/yyyy"),
                ExpiryDateStr = entity.ExpiryDate.ToString("dd/MM/yyyy"),
                RecordCreatedDtStr = entity.RecordCreatedDt.ToString("dd/MM/yyyy"),
                RecordModifiedDtStr = entity.RecordModifiedDt.ToString("dd/MM/yyyy"),
                RecordStatusUpdateDtStr = (entity.RecordStatusUpdateDt.HasValue) ? entity.RecordStatusUpdateDt.Value.ToString("dd/MM/yyyy") : "",
                PreferedBUStr = PBU,
                CobrandedTypeStr = CBCT
            };
        }

        /// <summary>
        /// model to database entity
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        internal tblCardMaster CopyFromModelToDb(CardMasterModel model)
        {
            return new tblCardMaster()
            {
                RecordID = model.RecordID,
                ActionFlags = model.ActionFlags,

                Name = model.Name.ToTrim(),
                NameOnCard = model.NameOnCard.ToTrim(),
                IDType = model.IDType.ToTrim(),
                IDNumber = model.IDNumber.ToTrim(),
                PAssionCardNumber = model.PAssionCardNumber,
                PreferredBU = model.PreferredBU.ToTrim(),
                EmailAddress = model.EmailAddress.ToTrim(),
                MA_BlockHouseNo = model.MA_BlockHouseNo.ToTrim(),
                MA_StreetName = model.MA_StreetName.ToTrim(),
                MA_FloorNo = model.MA_FloorNo.ToTrim(),
                MA_UnitNo = model.MA_UnitNo.ToTrim(),
                MA_BuildingName = model.MA_BuildingName.ToTrim(),
                MA_PostalCode = model.MA_PostalCode.ToTrim(),
                HA_BlockHouseNo = model.HA_BlockHouseNo.ToTrim(),
                HA_StreetName = model.HA_StreetName.ToTrim(),
                HA_FloorNo = model.HA_FloorNo.ToTrim(),
                HA_UnitNo = model.HA_UnitNo.ToTrim(),
                HA_BuildingName = model.HA_BuildingName.ToTrim(),
                HA_PostalCode = model.HA_PostalCode.ToTrim(),
                ExpiryDate = model.ExpiryDate,
                HpNumber = model.HpNumber.ToTrim(),
                ContactNumberHome = model.ContactNumberHome.ToTrim(),
                WV = model.WV.ToTrim(),
                PAYM = model.PAYM.ToTrim(),
                CoBranded = model.CoBranded.ToTrim(),
                CoBrandedCardType = model.CoBrandedCardType.ToTrim(),
                CardTypeFlag = model.CardTypeFlag.ToTrim(),
                Birthday = model.Birthday,
                Gender = model.Gender.ToTrim(),
                MaritalStatus = model.MaritalStatus.ToTrim(),
                AnnualIncome = model.AnnualIncome.ToTrim(),
                HousingType = model.HousingType.ToTrim(),
                HighestEducationLevel = model.HighestEducationLevel.ToTrim(),
                PrintBU = model.PrintBU.ToTrim(),
                PreferredWV = model.PreferredWV.ToTrim(),
                BillingBU = model.BillingBU.ToTrim(),
                OldPassionezLinkNumber = model.OldPassionezLinkNumber.ToTrim(),
                Nationality = model.Nationality.ToTrim(),
                Title = model.Title.ToTrim(),
                Race = model.Race.ToTrim(),
                Occupation = model.Occupation.ToTrim(),
                UserId = model.UserId,
                FileMasterId = model.FileMasterId,
                RecordCreatedDt = model.RecordCreatedDt,
                RecordModifiedDt = model.RecordModifiedDt,

                PictureUpdated = model.PictureUpdated,
                PictureFile = model.PictureFile,
                // Picture = model.Picture,
                SendInPrintFile = model.SendInPrintFile,
                PrintFileRefId = model.PrintFileRefId,
                CardCANNumber = model.CardCANNumber,
                CardCANExpiryDt = model.CardCANExpiryDt.ToTrim(),
                SendAck = model.SendAck,
                AckFileRefId = model.AckFileRefId,
                RecordStatus = string.IsNullOrEmpty(model.RecordStatus) ? Enums.Enum.RecordStatus.N.ToString() : model.RecordStatus.ToTrim(),
                RecordStatusUpdateDt = model.RecordStatusUpdateDt,

            };
        }
    }

    public class CardListPagingModel
    {
        public int page { get; set; }
        public int records { get; set; }
        public decimal total { get; set; }
        public List<CardMasterModel> data { get; set; }

        public CardListPagingModel()
        {
            data = new List<CardMasterModel>();
        }
    }

    public class CardTapListPagingModel
    {
        public int page { get; set; }
        public int records { get; set; }
        public decimal total { get; set; }
        public List<CardTappingModel> data { get; set; }

        public CardTapListPagingModel()
        {
            data = new List<CardTappingModel>();
        }
    }

    public class CardEditModel
    {
        public int RecordId { get; set; }
        [Required]
        public decimal CardCANNumber { get; set; }
        [Required]
        public string CardCANExpiryDt { get; set; }
    }

}