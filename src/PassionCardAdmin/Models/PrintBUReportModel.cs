﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionCardAdmin.Models
{
    public class PrintBUReportModelPagingModel
    {
        public int page { get; set; }
        public int records { get; set; }
        public decimal total { get; set; }
        public string Title { get; set; }
        public List<PrintBUParent1Model> data { get; set; }

        public PrintBUReportModelPagingModel()
        {
            data = new List<PrintBUParent1Model>();
        }
    }

    public class PrintBUParent1Model
    {
        public string Title { get; set; }
        public List<PrintBUParent2Model> cardModelList { get; set; }

        public PrintBUParent1Model()
        {
            cardModelList = new List<PrintBUParent2Model>();
        }
    }

    public class PrintBUParent2Model
    {
        public string Title { get; set; }
        public string PrintBUValue { get; set; }
        public List<CardMasterModel> cardModelList { get; set; }

        public PrintBUParent2Model()
        {
            cardModelList = new List<CardMasterModel>();
        }
    }

    public class PrintBUReportModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    public class PrintBUReportDataModel
    {
        public string RecordId { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public string IDNumber { get; set; }
        public string DOB { get; set; }
        public string PassionCardNumber { get; set; }
        public string ExpiryDate { get; set; }
        public string PrintBU { get; set; }
        public string RecordCreationDate { get; set; }
        public string PictureUploadDate { get; set; }

        public PrintBUReportDataModel CopyToReportModel(CardMasterModel model)
        {
            return new PrintBUReportDataModel()
            {
                DOB = model.BirthdayStr,
                ExpiryDate = model.ExpiryDateStr,
                Gender = (model.Gender == "001") ? "Female" : "Male",
                IDNumber = "TXXX"+ model.IDNumber.Substring(model.IDNumber.Length-5),
                Name = model.Name,
                PassionCardNumber = model.PAssionCardNumber.ToString(),
                PictureUploadDate = model.RecordStatusUpdateDtStr,
                PrintBU = model.PreferedBUStr,
                RecordCreationDate = model.RecordCreatedDtStr,
                RecordId = model.RecordID.ToString()
            };
        }
    }
}